﻿
namespace MuonRoiSocialNetwork.Common.Enums.Storys
{
    public enum EnumStoriesSpecial
    {
        StoriesSimilar,
        StoriesAll,
        StoriesNew,
        StoriesNewUpdate,
        StoriesComplete
    }
}
