﻿namespace MuonRoiSocialNetwork.Common.Models.Stories.Dtos
{
    public class StoryDataDto
    {
        public long StoryId { get; set; }
        public int MaxChapter { get; set; }
        public string Url { get; set; } = string.Empty;
        public long CategoryId { get; set; }
    }
}
