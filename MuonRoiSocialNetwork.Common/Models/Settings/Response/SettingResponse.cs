﻿
using MuonRoiSocialNetwork.Common.Enums.Settings;
using Newtonsoft.Json;

namespace MuonRoiSocialNetwork.Common.Models.Settings.Response
{
    public class SettingResponse
    {
        [JsonProperty("setting_name")]
        public string SettingName { get; set; } = string.Empty;
        [JsonProperty("conntent")]
        public string Content { get; set; } = string.Empty;
        [JsonProperty("setting_type")]
        public EnumSettingType Type { get; set; }
    }
}
