﻿using Newtonsoft.Json;

namespace MuonRoiSocialNetwork.Common.Models.Settings.Response
{
    public class SettingBannerResponse : SettingResponse
    {
        [JsonProperty("banner_url")]
        public IEnumerable<string> BannerUrl { get; set; } = new List<string>();
    }
}
