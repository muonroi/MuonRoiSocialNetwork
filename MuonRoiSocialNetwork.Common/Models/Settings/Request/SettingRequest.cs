﻿using MuonRoiSocialNetwork.Common.Enums.Settings;
using Newtonsoft.Json;

namespace MuonRoiSocialNetwork.Common.Models.Settings.Request
{
    public class SettingRequest
    {
        /// <summary>
        /// Setting name
        /// </summary>
        [JsonProperty("setting_name")]
        public string SettingName { get; set; } = string.Empty;
        /// <summary>
        /// Content setting
        /// </summary>
        [JsonProperty("conntent")]
        public string Content { get; set; } = string.Empty;
        /// <summary>
        /// Setting type
        /// </summary>
        [JsonProperty("type")]
        public EnumSettingType SettingType { get; set; }

    }
}
