﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace MuonRoiSocialNetwork.Common.Models.Settings.Request
{
    public class SettingBannerRequest : SettingRequest
    {
        /// <summary>
        /// Banner file
        /// </summary>
        [JsonProperty("banner_file")]
        public List<IFormFile>? BannerFile { get; set; } = new List<IFormFile>();
    }
}
