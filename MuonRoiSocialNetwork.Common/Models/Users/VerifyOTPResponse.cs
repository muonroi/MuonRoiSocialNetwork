﻿namespace MuonRoiSocialNetwork.Common.Models.Users
{
    public class VerifyOTPResponse
    {
        public bool IsVerify { get; set; }
        public string Token { get; set; } = string.Empty;
    }
}
