﻿using Newtonsoft.Json;

namespace MuonRoiSocialNetwork.Common.Models.Chapter.Dto
{
    public class CrawlChapterDto
    {
        [JsonProperty("chapterTitle")]
        public string ChapterTitle { get; set; } = string.Empty;
        [JsonProperty("body")]
        public string Body { get; set; } = string.Empty;
        [JsonProperty("numberOfChapter")]
        public long NumberOfChapter { get; set; }
        [JsonProperty("storyId")]
        public int StoryId { get; set; }
    }
}
