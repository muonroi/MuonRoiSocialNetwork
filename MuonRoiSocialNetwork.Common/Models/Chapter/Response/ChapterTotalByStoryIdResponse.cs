﻿namespace MuonRoiSocialNetwork.Common.Models.Chapter.Response
{
    public class ChapterTotalByStoryIdResponse
    {
        public int ChapterTotal { get; set; }
    }
}
