﻿namespace MuonRoiSocialNetwork.Common.Settings.RoleSettings
{
    public enum RoleSettings
    {
        SU, MOD, AUTHOR, VIEWER, CHANGEPASS
    }
}
