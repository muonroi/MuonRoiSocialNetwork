﻿namespace MuonRoiSocialNetwork.Common.Settings.Appsettings
{
    public sealed class ConstAppSettings
    {
        public ConstAppSettings()
        {

        }
        public static ConstAppSettings Instance { get { return Nested.instance; } }

        private class Nested
        {
            static Nested()
            {
            }

            internal static readonly ConstAppSettings instance = new();
        }
        public readonly string CONNECTIONSTRING_DB = "ConnectionStrings:MuonRoi";
        public readonly string CONNECTIONSTRING_HANGFIRE = "ConnectionStrings:HangfireConnection";
        public readonly string CONNECTIONSTRING_REDIS = "ConnectionStrings:Redis";
        public readonly string APPLICATIONSERECT = "Application:SERECT";
        public readonly string ENV_LIGHT_TAIL_ACCESSKEY = "Lightsail-aws:ENV_ACCESSKEY";
        public readonly string ENV_AWS_ACCESSKEY = "aws:ENV_ACCESSKEY";
        public readonly string APPLICATIONAPPDOMAIN = "Application:AppDomain";
        public readonly string APPLICATIONEMAILCONFIRMED = "Application:EmailConfirmation";
        public readonly string ENV_LIGHT_TAIL_SERECT = "Lightsail-aws:ENV_SERECT";
        public readonly string ENV_AWS_SERECT = "aws:ENV_SERECT";
        public readonly string LIFE_TIME = "Application:LIFE_TIME";
        public readonly string WHATSAPP_ACESSTOKEN = "Whatsapp:AcessToken";
        public readonly string PUBLICKEY_ENCRYPT = "Application:EncryptKey";
        public readonly string PUBLICKEY_IV = "Application:IvKey";

    }
}
