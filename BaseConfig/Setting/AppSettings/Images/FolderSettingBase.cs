﻿namespace BaseConfig.Setting.AppSettings.Images
{
    internal class FolderSettingBase
    {
        public static string DefaultNameImages = "muonroi_img";
        public const string IMG_STORY = "StoriesImages/";
        public const string IMG_USERS = "Users/";
        public const string BUCKET_NAME = "muonroi-image";
    }
}
