﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MuonRoiSocialNetwork.Common.Settings.Appsettings;
using Slugify;
using System.Collections;
using System.Globalization;
using System.IO.Compression;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace BaseConfig.Extentions.String
{
    public static class StringHelpers
    {
        private static readonly Random _randomTemp = new(DateTime.UtcNow.Millisecond);
        private static readonly SlugHelper _slugHelper = new();
        private static readonly ThreadLocal<RandomNumberGenerator> crng = new(RandomNumberGenerator.Create);
        private static readonly ThreadLocal<byte[]> bytes = new(() => new byte[sizeof(int)]);
        public static int NextInt()
        {
            crng.Value?.GetBytes(bytes.Value ?? new byte[0x56]);
            return BitConverter.ToInt32(bytes.Value ?? new byte[0x56], 0) & int.MaxValue;
        }
        public static bool IsUrlValid(string url)
        {
            try
            {
                return Uri.TryCreate(url, UriKind.Absolute, result: out Uri resultUri)
                  && (resultUri.Scheme == Uri.UriSchemeHttp || resultUri.Scheme == Uri.UriSchemeHttps);
            }
            catch (Exception)
            {
                return false;
            }

        }
        public static double NextDouble()
        {
            while (true)
            {
                long x = NextInt() & 0x001FFFFF;
                x <<= 31;
                x |= (long)NextInt();
                double n = x;
                const double d = 1L << 52;
                double q = n / d;
                if (q != 1.0)
                    return q;
            }
        }

        public static string ByteToString(this byte[] data)
        {
            return BitConverter.ToString(data);
        }
        public static byte[] StringToByte(this string data)
        {
            return Encoding.UTF8.GetBytes(data);
        }
        public static string EncryptStringAES(string plainText, IConfiguration configuration)
        {
            using var aesAlg = Aes.Create();
            aesAlg.Key = Convert.FromBase64String(configuration[ConstAppSettings.Instance.PUBLICKEY_ENCRYPT]);
            aesAlg.IV = Convert.FromBase64String(configuration[ConstAppSettings.Instance.PUBLICKEY_IV]);
            var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
            using var msEncrypt = new MemoryStream();
            using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
            {
                using var swEncrypt = new StreamWriter(csEncrypt);
                swEncrypt.Write(plainText);
            }

            return Convert.ToBase64String(msEncrypt.ToArray());
        }

        public static string EncryptData(string data, string serectKey)
        {
            string bitStrengthString = serectKey[..(serectKey.IndexOf("</BitStrength>") + 14)];
            serectKey = serectKey.Replace(bitStrengthString, "");
            int bitStrength = Convert.ToInt32(bitStrengthString.Replace("<BitStrength>", "").Replace("</BitStrength>", ""));
            return EncryptString(data, bitStrength, serectKey);
        }
        public static string EncryptString(string inputString, int dwKeySize, string xmlString)
        {
            var rsaCryptoServiceProvider = new RSACryptoServiceProvider(dwKeySize);
            rsaCryptoServiceProvider.FromXmlString(xmlString);
            int keySize = dwKeySize / 8;
            byte[] bytes = Encoding.UTF32.GetBytes(inputString);
            int maxLength = keySize - 42;
            int dataLength = bytes.Length;
            int iterations = dataLength / maxLength;
            var stringBuilder = new StringBuilder();
            for (int i = 0; i <= iterations; i++)
            {
                byte[] tempBytes = new byte[(dataLength - maxLength * i > maxLength) ? maxLength : dataLength - maxLength * i];
                Buffer.BlockCopy(bytes, maxLength * i, tempBytes, 0, tempBytes.Length);
                byte[] encryptedBytes = rsaCryptoServiceProvider.Encrypt(tempBytes, true);
                Array.Reverse(encryptedBytes);
                stringBuilder.Append(Convert.ToBase64String(encryptedBytes));
            }
            return stringBuilder.ToString();
        }

        public static string DecryptString(string inputString, int dwKeySize, string xmlString)
        {
            var rsaCryptoServiceProvider = new RSACryptoServiceProvider(dwKeySize);
            rsaCryptoServiceProvider.FromXmlString(xmlString);
            int base64BlockSize = ((dwKeySize / 8) % 3 != 0) ? (((dwKeySize / 8) / 3) * 4) + 4 : ((dwKeySize / 8) / 3) * 4;
            int iterations = inputString.Length / base64BlockSize;
            var arrayList = new ArrayList();
            for (int i = 0; i < iterations; i++)
            {
                byte[] encryptedBytes = Convert.FromBase64String(inputString.Substring(base64BlockSize * i, base64BlockSize));
                Array.Reverse(encryptedBytes);
                arrayList.AddRange(rsaCryptoServiceProvider.Decrypt(encryptedBytes, true));
            }
            return Encoding.UTF32.GetString(arrayList.ToArray(Type.GetType("System.Byte")) as byte[]);
        }
        public static string WithRegex(string text)
        {
            return Regex.Replace(text, @"\s+", " ");
        }
        public static long GenerateOtp()
        {
            HashSet<long> numberOtps = new();
            HashSet<long> fourDigitNumbers = new();

            for (long i = 0; i < 100; i++)
            {
                numberOtps.Add(_randomTemp.Next(9, 999));
            }

            foreach (long number in numberOtps)
            {
                if (number >= 10 && number <= 99)
                {
                    long digit1 = number / 10;
                    long digit2 = number % 10;

                    for (long i = 0; i < numberOtps.Count; i++)
                    {
                        for (long j = 0; j < numberOtps.Count; j++)
                        {
                            long fourDigitNumber = digit1 * 1000 + digit2 * 100 + i * 10 + j;
                            if (fourDigitNumber >= 1000 && fourDigitNumber <= 9999)
                            {
                                fourDigitNumbers.Add(fourDigitNumber);
                            }
                        }
                    }
                }
                else if (number >= 100 && number <= 999)
                {
                    long digit1 = number / 100;
                    long remainingDigits = number % 100;

                    for (long i = 0; i < numberOtps.Count; i++)
                    {
                        long fourDigitNumber = digit1 * 1000 + remainingDigits * 10 + i;
                        if (fourDigitNumber >= 1000 && fourDigitNumber <= 9999)
                        {
                            fourDigitNumbers.Add(fourDigitNumber);
                        }
                    }
                }
            }
            List<long> tempFourDigitNumber = fourDigitNumbers.ToList().Where(x => x >= 1000 && x.ToString()[0] != 0).Select(x => x).ToList();
            int indexRandom = _randomTemp.Next(0, tempFourDigitNumber.Count - 1);

            return tempFourDigitNumber.IndexOf(indexRandom);
        }
        static public string EncodeTo64(this string toEncode)
        {
            byte[] toEncodeAsBytes = Encoding.ASCII.GetBytes(toEncode);

            string returnValue = Convert.ToBase64String(toEncodeAsBytes);

            return returnValue;
        }
        static public string DecodeFrom64(this string encodedData)
        {
            byte[] encodedDataAsBytes = Convert.FromBase64String(encodedData);

            string returnValue = Encoding.ASCII.GetString(encodedDataAsBytes);

            return returnValue;
        }
        /// <summary>
        /// Nomarlize text after genarate text to slug
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private static string NormalizeString(this string input)
        {
            input = input.ToLower().Replace('đ', 'd');
            var normalizedStringBuilder = new StringBuilder();
            foreach (char c in input.Normalize(NormalizationForm.FormD))
            {
                if (CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                {
                    normalizedStringBuilder.Append(c);
                }
            }
            return normalizedStringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }
        static public string GenerateSlug(this string textString)
        {
            string? slug;
            try
            {
                slug = _slugHelper.GenerateSlug(textString.NormalizeString());
            }
            catch (Exception)
            {
                slug = "khong-de";
            }
            return string.IsNullOrEmpty(slug) ? "khong-de" : slug;
        }
        public static string CompressHtml(string html)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(html);

            using var memoryStream = new MemoryStream();
            using (GZipStream gzipStream = new(memoryStream, CompressionMode.Compress, true))
            {
                gzipStream.Write(buffer, 0, buffer.Length);
            }

            return Convert.ToBase64String(memoryStream.ToArray());
        }

        public static string DecompressHtml(string compressedHtml)
        {
            byte[] buffer = Convert.FromBase64String(compressedHtml);

            using var memoryStream = new MemoryStream(buffer);
            using var gzipStream = new GZipStream(memoryStream, CompressionMode.Decompress);
            using var reader = new StreamReader(gzipStream);
            return reader.ReadToEnd();
        }
        public static CookieContainer ParseCookies(string cookieString, string domain)
        {
            var cookies = new CookieContainer();

            string[] cookiePairs = cookieString.Split(';');

            foreach (string cookiePair in cookiePairs)
            {
                string[] parts = cookiePair.Trim().Split('=');
                if (parts.Length == 2)
                {
                    string cookieName = parts[0].Trim();
                    string cookieValue = parts[1].Trim();
                    var cookie = new Cookie(cookieName, cookieValue, "/", domain);
                    cookies.Add(cookie);
                }
            }

            return cookies;
        }
        public static string RemovePostFix(this string str, params string[] postFixes)
        {
            if (str == null)
            {
                return null;
            }

            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }

            if (postFixes.IsNullOrEmpty())
            {
                return str;
            }

            foreach (var postFix in postFixes)
            {
                if (str.EndsWith(postFix))
                {
                    return str.Left(str.Length - postFix.Length);
                }
            }

            return str;
        }
        public static string Left(this string str, int len)
        {
            if (str == null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (str.Length < len)
            {
                throw new ArgumentException("len argument can not be bigger than given string's length!");
            }

            return str.Substring(0, len);
        }
    }

}
