﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using BaseConfig.Setting.AppSettings.Images;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using MuonRoiSocialNetwork.Common.Settings.Appsettings;
using SixLabors.ImageSharp.Formats.Png;

namespace BaseConfig.Extentions.ImageHelper
{
    public class ImageHelper
    {
        public async static Task<Dictionary<string, string>> Upload(IConfiguration configuration, IFormFile image)
        {
            //160x240
            if (image == null || image.Length <= 0) return null;
            string accessKey = configuration.GetSection(ConstAppSettings.Instance.ENV_LIGHT_TAIL_ACCESSKEY).Value;
            string secretKey = configuration.GetSection(ConstAppSettings.Instance.ENV_LIGHT_TAIL_SERECT).Value;
            using MemoryStream memoryStream = new();
            await image.CopyToAsync(memoryStream).ConfigureAwait(false);
            string bucketName = FolderSettingBase.BUCKET_NAME;
            using var s3Client = new AmazonS3Client(accessKey, secretKey, RegionEndpoint.USEast1);
            var request = new PutObjectRequest
            {
                BucketName = bucketName,
                Key = FolderSettingBase.IMG_STORY + FolderSettingBase.DefaultNameImages + "_" + image.FileName,
                InputStream = memoryStream
            };
            var response = await s3Client.PutObjectAsync(request).ConfigureAwait(false);
            Dictionary<string, string> temp = new()
            {
                {request.Key, response.HttpStatusCode.ToString() }
            };
            return temp;
        }
        public static string GetUrl(IConfiguration configuration, string imageName)
        {
            if (imageName == null || string.IsNullOrEmpty(imageName)) return null;

            string accessKey = configuration.GetSection(ConstAppSettings.Instance.ENV_LIGHT_TAIL_ACCESSKEY).Value;
            string secretKey = configuration.GetSection(ConstAppSettings.Instance.ENV_LIGHT_TAIL_SERECT).Value;
            string bucket = FolderSettingBase.BUCKET_NAME;
            using var s3Client = new AmazonS3Client(accessKey, secretKey, RegionEndpoint.USEast1);
            var request = new GetPreSignedUrlRequest
            {
                BucketName = bucket,
                Key = imageName,
                Expires = DateTime.UtcNow.AddDays(30)
            };
            string url = s3Client.GetPreSignedURL(request);
            return url;
        }
        public static async Task<IFormFile> ConvertAndCompressImageAsync(string userGuid, IFormFile file, int width, int height, bool isCalcHeight = true, PngCompressionLevel level = PngCompressionLevel.Level5)
        {
            using var imageStream = file.OpenReadStream();
            using var image = await Image.LoadAsync(imageStream);
            int newHeight = isCalcHeight ? (int)(((float)height / image.Height) * width) : height;
            image.Mutate(x => x.Resize(new ResizeOptions { Size = new Size(width, newHeight), Mode = ResizeMode.Max }));
            var encoder = new PngEncoder { CompressionLevel = level };
            var compressedStream = new MemoryStream();
            image.Save(compressedStream, encoder);
            var compressedFile = new FormFile(compressedStream, 0, compressedStream.Length, $"{userGuid}{DateTime.UtcNow.Millisecond}.{file.ContentType.Split('/').Last()}", $"{userGuid}{DateTime.UtcNow.Millisecond}.{file.ContentType.Split('/').Last()}");
            return compressedFile;
        }
    }
}
