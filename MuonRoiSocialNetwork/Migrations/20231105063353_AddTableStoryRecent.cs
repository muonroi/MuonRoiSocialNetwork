﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace MuonRoiSocialNetwork.Migrations
{
    public partial class AddTableStoryRecent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "storyrecent",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    guid = table.Column<Guid>(type: "uuid", nullable: false),
                    created_user_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    updated_user_guid = table.Column<Guid>(type: "uuid", nullable: true),
                    deleted_user_guid = table.Column<Guid>(type: "uuid", nullable: true),
                    created_username = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    updated_username = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    deleted_username = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    created_date_ts = table.Column<double>(type: "double precision", nullable: true),
                    updated_date_ts = table.Column<double>(type: "double precision", nullable: true),
                    deleted_date_ts = table.Column<double>(type: "double precision", nullable: true),
                    is_deleted = table.Column<bool>(type: "boolean", nullable: false),
                    story_id = table.Column<long>(type: "bigint", nullable: false),
                    user_guid = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_storyrecent", x => x.id);
                    table.ForeignKey(
                        name: "FK_storyrecent_appuser_user_guid",
                        column: x => x.user_guid,
                        principalTable: "appuser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_storyrecent_story_story_id",
                        column: x => x.story_id,
                        principalTable: "story",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_storyrecent_story_id",
                table: "storyrecent",
                column: "story_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_storyrecent_story_id_user_guid_id",
                table: "storyrecent",
                columns: new[] { "story_id", "user_guid", "id" });

            migrationBuilder.CreateIndex(
                name: "IX_storyrecent_user_guid",
                table: "storyrecent",
                column: "user_guid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "storyrecent");
        }
    }
}
