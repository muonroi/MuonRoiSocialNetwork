﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MuonRoiSocialNetwork.Migrations
{
    public partial class AddUserSubscription2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserSubscription_appuser_user_guid",
                table: "UserSubscription");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserSubscription",
                table: "UserSubscription");

            migrationBuilder.RenameTable(
                name: "UserSubscription",
                newName: "usersubscription");

            migrationBuilder.RenameIndex(
                name: "IX_UserSubscription_user_guid",
                table: "usersubscription",
                newName: "IX_usersubscription_user_guid");

            migrationBuilder.AddPrimaryKey(
                name: "PK_usersubscription",
                table: "usersubscription",
                column: "id");

            migrationBuilder.AddForeignKey(
                name: "FK_usersubscription_appuser_user_guid",
                table: "usersubscription",
                column: "user_guid",
                principalTable: "appuser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_usersubscription_appuser_user_guid",
                table: "usersubscription");

            migrationBuilder.DropPrimaryKey(
                name: "PK_usersubscription",
                table: "usersubscription");

            migrationBuilder.RenameTable(
                name: "usersubscription",
                newName: "UserSubscription");

            migrationBuilder.RenameIndex(
                name: "IX_usersubscription_user_guid",
                table: "UserSubscription",
                newName: "IX_UserSubscription_user_guid");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserSubscription",
                table: "UserSubscription",
                column: "id");

            migrationBuilder.AddForeignKey(
                name: "FK_UserSubscription_appuser_user_guid",
                table: "UserSubscription",
                column: "user_guid",
                principalTable: "appuser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
