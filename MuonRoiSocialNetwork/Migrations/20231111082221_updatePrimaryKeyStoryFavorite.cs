﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MuonRoiSocialNetwork.Migrations
{
    public partial class UpdatePrimaryKeyStoryFavorite : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_storyfavorite",
                table: "storyfavorite");

            migrationBuilder.AddPrimaryKey(
                name: "PK_storyfavorite",
                table: "storyfavorite",
                column: "id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_storyfavorite",
                table: "storyfavorite");

            migrationBuilder.AddPrimaryKey(
                name: "PK_storyfavorite",
                table: "storyfavorite",
                columns: new[] { "story_id", "user_guid" });
        }
    }
}
