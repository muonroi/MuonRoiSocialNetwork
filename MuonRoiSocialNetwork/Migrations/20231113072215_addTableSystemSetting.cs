﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace MuonRoiSocialNetwork.Migrations
{
    public partial class addTableSystemSetting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "systemsettings",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    guid = table.Column<Guid>(type: "uuid", nullable: false),
                    created_user_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    updated_user_guid = table.Column<Guid>(type: "uuid", nullable: true),
                    deleted_user_guid = table.Column<Guid>(type: "uuid", nullable: true),
                    created_username = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    updated_username = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    deleted_username = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    created_date_ts = table.Column<double>(type: "double precision", nullable: true),
                    updated_date_ts = table.Column<double>(type: "double precision", nullable: true),
                    deleted_date_ts = table.Column<double>(type: "double precision", nullable: true),
                    is_deleted = table.Column<bool>(type: "boolean", nullable: false),
                    SettingName = table.Column<string>(type: "text", nullable: false),
                    Content = table.Column<string>(type: "text", nullable: false),
                    Type = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_systemsettings", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "systemsettings");
        }
    }
}
