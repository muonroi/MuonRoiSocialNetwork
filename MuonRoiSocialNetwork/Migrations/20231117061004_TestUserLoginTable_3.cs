﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace MuonRoiSocialNetwork.Migrations
{
    public partial class TestUserLoginTable_3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserLogin_appuser_user_id",
                table: "UserLogin");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserLogin",
                table: "UserLogin");

            migrationBuilder.RenameTable(
                name: "UserLogin",
                newName: "userlogin");

            migrationBuilder.RenameIndex(
                name: "IX_UserLogin_user_id",
                table: "userlogin",
                newName: "IX_userlogin_user_id");

            migrationBuilder.AddColumn<double>(
                name: "created_date_ts",
                table: "language",
                type: "double precision",
                nullable: true)
                .Annotation("Relational:ColumnOrder", 107);

            migrationBuilder.AddColumn<Guid>(
                name: "created_user_guid",
                table: "language",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"))
                .Annotation("Relational:ColumnOrder", 101);

            migrationBuilder.AddColumn<string>(
                name: "created_username",
                table: "language",
                type: "character varying(100)",
                maxLength: 100,
                nullable: true)
                .Annotation("Relational:ColumnOrder", 104);

            migrationBuilder.AddColumn<double>(
                name: "deleted_date_ts",
                table: "language",
                type: "double precision",
                nullable: true)
                .Annotation("Relational:ColumnOrder", 109);

            migrationBuilder.AddColumn<Guid>(
                name: "deleted_user_guid",
                table: "language",
                type: "uuid",
                nullable: true)
                .Annotation("Relational:ColumnOrder", 103);

            migrationBuilder.AddColumn<string>(
                name: "deleted_username",
                table: "language",
                type: "character varying(100)",
                maxLength: 100,
                nullable: true)
                .Annotation("Relational:ColumnOrder", 106);

            migrationBuilder.AddColumn<Guid>(
                name: "guid",
                table: "language",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"))
                .Annotation("Relational:ColumnOrder", 1);

            migrationBuilder.AddColumn<long>(
                name: "id",
                table: "language",
                type: "bigint",
                nullable: false,
                defaultValue: 0L)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn)
                .Annotation("Relational:ColumnOrder", 0);

            migrationBuilder.AddColumn<bool>(
                name: "is_deleted",
                table: "language",
                type: "boolean",
                nullable: false,
                defaultValue: false)
                .Annotation("Relational:ColumnOrder", 110);

            migrationBuilder.AddColumn<double>(
                name: "updated_date_ts",
                table: "language",
                type: "double precision",
                nullable: true)
                .Annotation("Relational:ColumnOrder", 108);

            migrationBuilder.AddColumn<Guid>(
                name: "updated_user_guid",
                table: "language",
                type: "uuid",
                nullable: true)
                .Annotation("Relational:ColumnOrder", 102);

            migrationBuilder.AddColumn<string>(
                name: "updated_username",
                table: "language",
                type: "character varying(100)",
                maxLength: 100,
                nullable: true)
                .Annotation("Relational:ColumnOrder", 105);

            migrationBuilder.AddPrimaryKey(
                name: "PK_userlogin",
                table: "userlogin",
                column: "id");

            migrationBuilder.AddForeignKey(
                name: "FK_userlogin_appuser_user_id",
                table: "userlogin",
                column: "user_id",
                principalTable: "appuser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_userlogin_appuser_user_id",
                table: "userlogin");

            migrationBuilder.DropPrimaryKey(
                name: "PK_userlogin",
                table: "userlogin");

            migrationBuilder.DropColumn(
                name: "created_date_ts",
                table: "language");

            migrationBuilder.DropColumn(
                name: "created_user_guid",
                table: "language");

            migrationBuilder.DropColumn(
                name: "created_username",
                table: "language");

            migrationBuilder.DropColumn(
                name: "deleted_date_ts",
                table: "language");

            migrationBuilder.DropColumn(
                name: "deleted_user_guid",
                table: "language");

            migrationBuilder.DropColumn(
                name: "deleted_username",
                table: "language");

            migrationBuilder.DropColumn(
                name: "guid",
                table: "language");

            migrationBuilder.DropColumn(
                name: "id",
                table: "language");

            migrationBuilder.DropColumn(
                name: "is_deleted",
                table: "language");

            migrationBuilder.DropColumn(
                name: "updated_date_ts",
                table: "language");

            migrationBuilder.DropColumn(
                name: "updated_user_guid",
                table: "language");

            migrationBuilder.DropColumn(
                name: "updated_username",
                table: "language");

            migrationBuilder.RenameTable(
                name: "userlogin",
                newName: "UserLogin");

            migrationBuilder.RenameIndex(
                name: "IX_userlogin_user_id",
                table: "UserLogin",
                newName: "IX_UserLogin_user_id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserLogin",
                table: "UserLogin",
                column: "id");

            migrationBuilder.AddForeignKey(
                name: "FK_UserLogin_appuser_user_id",
                table: "UserLogin",
                column: "user_id",
                principalTable: "appuser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
