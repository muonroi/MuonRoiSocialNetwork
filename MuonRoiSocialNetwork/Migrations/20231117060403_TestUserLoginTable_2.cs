﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MuonRoiSocialNetwork.Migrations
{
    public partial class TestUserLoginTable_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserLoggins_appuser_user_id",
                table: "UserLoggins");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserLoggins",
                table: "UserLoggins");

            migrationBuilder.RenameTable(
                name: "UserLoggins",
                newName: "UserLogin");

            migrationBuilder.RenameIndex(
                name: "IX_UserLoggins_user_id",
                table: "UserLogin",
                newName: "IX_UserLogin_user_id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserLogin",
                table: "UserLogin",
                column: "id");

            migrationBuilder.AddForeignKey(
                name: "FK_UserLogin_appuser_user_id",
                table: "UserLogin",
                column: "user_id",
                principalTable: "appuser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserLogin_appuser_user_id",
                table: "UserLogin");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserLogin",
                table: "UserLogin");

            migrationBuilder.RenameTable(
                name: "UserLogin",
                newName: "UserLoggins");

            migrationBuilder.RenameIndex(
                name: "IX_UserLogin_user_id",
                table: "UserLoggins",
                newName: "IX_UserLoggins_user_id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserLoggins",
                table: "UserLoggins",
                column: "id");

            migrationBuilder.AddForeignKey(
                name: "FK_UserLoggins_appuser_user_id",
                table: "UserLoggins",
                column: "user_id",
                principalTable: "appuser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
