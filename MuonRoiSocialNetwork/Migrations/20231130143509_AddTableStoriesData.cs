﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace MuonRoiSocialNetwork.Migrations
{
    public partial class AddTableStoriesData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "storiesdata",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    guid = table.Column<Guid>(type: "uuid", nullable: false),
                    created_user_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    updated_user_guid = table.Column<Guid>(type: "uuid", nullable: true),
                    deleted_user_guid = table.Column<Guid>(type: "uuid", nullable: true),
                    created_username = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    updated_username = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    deleted_username = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    created_date_ts = table.Column<double>(type: "double precision", nullable: true),
                    updated_date_ts = table.Column<double>(type: "double precision", nullable: true),
                    deleted_date_ts = table.Column<double>(type: "double precision", nullable: true),
                    is_deleted = table.Column<bool>(type: "boolean", nullable: false),
                    StoryId = table.Column<long>(type: "bigint", nullable: false),
                    MaxChapter = table.Column<int>(type: "integer", nullable: false),
                    Url = table.Column<string>(type: "text", nullable: false),
                    CategoryId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_storiesdata", x => x.id);
                    table.ForeignKey(
                        name: "FK_storiesdata_category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "category",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_storiesdata_story_StoryId",
                        column: x => x.StoryId,
                        principalTable: "story",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_storiesdata_CategoryId",
                table: "storiesdata",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_storiesdata_StoryId",
                table: "storiesdata",
                column: "StoryId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "storiesdata");
        }
    }
}
