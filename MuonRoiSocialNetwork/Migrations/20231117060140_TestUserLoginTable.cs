﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MuonRoiSocialNetwork.Migrations
{
    public partial class TestUserLoginTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_userlogin_appuser_user_id",
                table: "userlogin");

            migrationBuilder.DropPrimaryKey(
                name: "PK_userlogin",
                table: "userlogin");

            migrationBuilder.RenameTable(
                name: "userlogin",
                newName: "UserLoggins");

            migrationBuilder.RenameIndex(
                name: "IX_userlogin_user_id",
                table: "UserLoggins",
                newName: "IX_UserLoggins_user_id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserLoggins",
                table: "UserLoggins",
                column: "id");

            migrationBuilder.AddForeignKey(
                name: "FK_UserLoggins_appuser_user_id",
                table: "UserLoggins",
                column: "user_id",
                principalTable: "appuser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserLoggins_appuser_user_id",
                table: "UserLoggins");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserLoggins",
                table: "UserLoggins");

            migrationBuilder.RenameTable(
                name: "UserLoggins",
                newName: "userlogin");

            migrationBuilder.RenameIndex(
                name: "IX_UserLoggins_user_id",
                table: "userlogin",
                newName: "IX_userlogin_user_id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_userlogin",
                table: "userlogin",
                column: "id");

            migrationBuilder.AddForeignKey(
                name: "FK_userlogin_appuser_user_id",
                table: "userlogin",
                column: "user_id",
                principalTable: "appuser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
