﻿using Hangfire;
using Hangfire.Annotations;
using Hangfire.Dashboard;
using MuonRoiSocialNetwork.Infrastructure.Crawl;
using MuonRoiSocialNetwork.Infrastructure.Jobs;

namespace MuonRoiSocialNetwork.StartupConfig
{
    internal static class CustomHangfireJobConfiguration
    {
        public static void RegisterJobs()
        {
            //RecurringJob.AddOrUpdate<CrawlStoriesOnYY>("health-check-chapter", x => x.CheckDailyChapter(), "0 0 */30 * *");
            //RecurringJob.AddOrUpdate<UpdateChapterTotalJob>("health-update-total-chapter", x => x.Execute(), "0 0 */30 * *");
        }
    }
    internal class HangfireJwtAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize([NotNull] DashboardContext context)
        {
            var httpContext = context.GetHttpContext();

            return httpContext.User.Identity?.IsAuthenticated ?? false;
        }
    }
}
