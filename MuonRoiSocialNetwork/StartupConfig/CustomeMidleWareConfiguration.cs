﻿using Hangfire;
using Hangfire.Dashboard;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using MuonRoiSocialNetwork.Infrastructure;
using MuonRoiSocialNetwork.Infrastructure.HubCentral;

namespace MuonRoiSocialNetwork.StartupConfig
{
    internal static class CustomeMidleWareConfiguration
    {
        public static WebApplication CustomMidleware(this WebApplication app)
        {
            var env = app.Services.GetRequiredService<IWebHostEnvironment>();
            app.UseSwagger();
            app.UseSwaggerUI();
            app.UseHttpsRedirection();
            app.UseRouting();
            app.MapControllers();
            app.UseStatusCodePages();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseCors();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapGet("/", context =>
                {
                    if (!(context.User.Identity?.IsAuthenticated) ?? false)
                    {
                        context.Response.Redirect("/home");
                    }
                    else
                    {
                        context.Response.Redirect("/swagger");
                    }
                    return Task.CompletedTask;
                });
                endpoints.MapHangfireDashboard(
                        options: new DashboardOptions
                        {
                            Authorization = new[] { new HangfireJwtAuthorizationFilter() },
                        });
                endpoints.MapHub<NotificationHub>("/hub/notification");
            });
            CustomHangfireJobConfiguration.RegisterJobs();

            using (var scope = app.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var context = services.GetRequiredService<MuonRoiSocialNetworkDbContext>();
                context.Database.Migrate();
            }
            return app;
        }
    }
}
