﻿using Bogus;
using Cloudflare_Evader;
using Hangfire;
using HtmlAgilityPack;
using MediatR;
using MuonRoi.Social_Network.Storys;
using MuonRoiSocialNetwork.Application.Commands.Chapter;
using MuonRoiSocialNetwork.Common.Settings.Appsettings;
using MuonRoiSocialNetwork.Domains.Interfaces.Commands.Stories;
using MuonRoiSocialNetwork.Domains.Interfaces.Queries.Category;
using MuonRoiSocialNetwork.Domains.Interfaces.Queries.Chapters;
using MuonRoiSocialNetwork.Domains.Interfaces.Repository.StoriesData;
using Npgsql;
using System.Net;
using System.Text.RegularExpressions;

namespace MuonRoiSocialNetwork.Infrastructure.Jobs
{
    /// <summary>
    /// Update total chapter 
    /// </summary>
    public class UpdateChapterTotalJob
    {
        private readonly IStoriesRepository _storiesRepository;
        private readonly IChapterQueries _chapterQueries;
        private readonly ICategoryQueries _categoryQueries;
        private readonly IConfiguration _configuration;
        private readonly IStoryDataRepository _storyDataRepository;
        private readonly IMediator _mediator;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="storiesRepository"></param>
        /// <param name="chapterQueries"></param>
        /// <param name="categoryQueries"></param>
        /// <param name="storyDataRepository"></param>
        /// <param name="configuration"></param>
        /// <param name="mediator"></param>
        public UpdateChapterTotalJob(IStoriesRepository storiesRepository, IChapterQueries chapterQueries, ICategoryQueries categoryQueries, IStoryDataRepository storyDataRepository, IConfiguration configuration, IMediator mediator)
        {
            _storiesRepository = storiesRepository;
            _chapterQueries = chapterQueries;
            _categoryQueries = categoryQueries;
            _storyDataRepository = storyDataRepository;
            _configuration = configuration;
            _mediator = mediator;
        }
        /// <summary>
        /// Fill total chapter to story and update new chapter
        /// </summary>
        /// <returns></returns>
        [JobDisplayName("total-chapter-set-is-complete")]
        public async Task Execute()
        {
            var categories = await _categoryQueries.GetAllAsync();
            foreach (var category in categories)
            {
                var stories = await _storiesRepository.GetWhereAsync(x => x.CategoryId == category.Id);
                foreach (var story in stories)
                {
                    var storyData = await _storyDataRepository.GetWhereFirstOrDefaultAsync(x => x.StoryId == story.Id);
                    if (storyData != null)
                    {
                        var storyLink = storyData.Url.Replace("/chuong-1.html", string.Empty);
                        var chapterByStory = await _chapterQueries.GetTotalChapterByStoryId(story.Id);
                        var realityChapterTotal = chapterByStory.Result;
                        await FillTotalChapter(story.StoryTitle, storyLink, realityChapterTotal.ChapterTotal, story);
                    }
                }
            }
        }
        private async Task FillTotalChapter(string storyName, string url, int totalChapter, Story oldStory)
        {
            try
            {
                string cookieString = "cf_clearance=fwVoIQIQUIw97LwZkzLzu2KmkkubZ2JSS4RJVAg7OxU-1697890814-0-1-c4d1f544.6a7b9b77.42fade56-250.0.0; messages=W1siX19qc29uX21lc3NhZ2UiLDAsMjUsIlN1Y2Nlc3NmdWxseSBzaWduZWQgaW4gYXMgTXVvbnJvaWlpLiJdXQ:1qx7Gy:cACuDhQnhWiRsveD45zx_HWXRNyGJe0yQUBOdAQZMws; csrftoken=sOaq0NOCEVSjF6PvZPdg8Map0m3d5RBfAVpQ6AWW2phg5KkfDDA7IdWnbgivIC2d; truyenyyid=8weu7kkvlpgggi11t3onz8vsfvzz09it";
                string domain = "truyenyy.pro";
                _ = ParseCookies(cookieString, domain);
                WebClient? webClient = null;
                var userAgent = GenerateUserAgent();
                while (webClient == null)
                {
                    Console.WriteLine($"{storyName} => trying...");
                    webClient = CloudflareEvader.CreateBypassedWebClient(userAgent, url);
                }
                webClient.Headers.Add(HttpRequestHeader.UserAgent, userAgent);
                webClient.Headers.Add(HttpRequestHeader.Cookie, cookieString);
                string content = await webClient.DownloadStringTaskAsync(url);
                var regexNumber = new Regex("\\d+(\\.\\d+)?");
                var document = new HtmlDocument();
                document.LoadHtml(content);
                var story = document.DocumentNode.SelectSingleNode("//div[@class='novel-info']");
                if (story == null)
                {
                    Console.WriteLine($"No stories found. at url {url}");
                    return;
                }
                var maxChapterOnWebsite = story.SelectSingleNode("//ul[@class='numbers list-unstyled']/li[1]");
                if (maxChapterOnWebsite == null)
                {
                    return;
                }
                _ = int.TryParse(regexNumber.Match(maxChapterOnWebsite.InnerText).Groups[0].Value, out int chapterNumber);
                if (totalChapter == chapterNumber)
                {
                    oldStory.TotalChapter = totalChapter;
                    oldStory.IsComplete = true;
                    _storiesRepository.Update(oldStory);
                    await _storiesRepository.UnitOfWork.SaveEntitiesAsync();
                }
                else
                {
                    oldStory.TotalChapter = totalChapter;
                    oldStory.IsComplete = false;
                    _storiesRepository.Update(oldStory);
                    await _storiesRepository.UnitOfWork.SaveEntitiesAsync();
                    // await CheckConsecutiveNumberOfChapter(url, oldStory.Id, chapterNumber);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        /// <summary>
        /// Check chapter missing
        /// </summary>
        /// <param name="urls"></param>
        /// <param name="storyId"></param>
        /// <param name="totalChapter"></param>
        /// <returns></returns>
        private async Task CheckConsecutiveNumberOfChapter(string urls, long storyId, int totalChapter)
        {
            var detectNameStory = new Regex("\\/truyen\\/([^\\/]+)\\/");
            var storyName = detectNameStory.Match(urls).Groups[1].Value;
            string connectionString = _configuration[ConstAppSettings.Instance.CONNECTIONSTRING_DB] ?? ConstAppSettings.Instance.CONNECTIONSTRING_DB;
            var baseUrl = "https://truyenyy.pro/truyen/{0}/chuong-{1}.html";
            using var connection = new NpgsqlConnection(connectionString);
            connection.Open();
            string checkConsecutiveChapter = $@"
                WITH ChapterRange AS (
                SELECT story_id, generate_series(1, {totalChapter}) AS all_chapters
                FROM chapter c 
                WHERE c.story_id = {storyId}
                GROUP BY c.story_id)
                SELECT  all_chapters AS missing_chapter
                FROM ChapterRange 
                WHERE all_chapters NOT IN (SELECT c.number_of_chapter FROM chapter c WHERE c.story_id = {storyId});";
            using (var command = new NpgsqlCommand(checkConsecutiveChapter, connection))
            {
                using var reader = await command.ExecuteReaderAsync();
                var chapterNumbers = new List<int>();
                while (reader.Read())
                {
                    int chapterNumber = reader.GetInt32(0);
                    chapterNumbers.Add(chapterNumber);
                }

                foreach (int singleChapter in chapterNumbers)
                {
                    var url = string.Format(baseUrl, storyName, singleChapter);
                    try
                    {
                        using HttpClient client = new();
                        var response = await client.GetAsync(url);
                        if (response.IsSuccessStatusCode)
                        {
                            var content = await response.Content.ReadAsStringAsync();
                            var document = new HtmlDocument();
                            document.LoadHtml(content);
                            var chapterNumberRaw = document?.DocumentNode.Descendants("h1").Where(d => d.Attributes["class"].Value.Contains("chap-title")).FirstOrDefault();
                            var title = document?.DocumentNode.SelectSingleNode(".//div[@class='chapter']/div/h2");
                            var contents = document?.DocumentNode.SelectSingleNode("//*[@id=\"inner_chap_content_1\"]");
                            if (contents is null)
                            {
                                Console.WriteLine($"{storyName} => No content found - {storyId}");
                                return;
                            }
                            var chapterNumber = chapterNumberRaw != null ? Regex.Match(chapterNumberRaw.SelectSingleNode("span").InnerText, @"\d+").Value : "-1";
                            var chapterResult = new CreateChapterCommand
                            {
                                NumberOfChapter = int.Parse(chapterNumber),
                                ChapterTitle = title is null || string.IsNullOrEmpty(title.InnerText.Trim()) ? "Không đề" : title.InnerText.Trim(),
                                Body = contents is null || string.IsNullOrEmpty(contents.InnerHtml.Trim()) ? "Chưa cập nhật nội dung" : contents.InnerHtml.Trim(),
                                StoryId = (int)storyId
                            };

                            if (chapterResult.Body.Length >= 750)
                            {
                                await _mediator.Send(chapterResult).ConfigureAwait(false);
                                Console.WriteLine($"{storyName} => Chap {chapterNumber} [{title?.InnerText.Trim()}] - {storyId}");
                            }
                        }


                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            connection.Close();
        }

        /// <summary>
        /// Parse cookies
        /// </summary>
        /// <param name="cookieString"></param>
        /// <param name="domain"></param>
        /// <returns></returns>
        public static CookieContainer ParseCookies(string cookieString, string domain)
        {
            var cookies = new CookieContainer();

            string[] cookiePairs = cookieString.Split(';');

            foreach (string cookiePair in cookiePairs)
            {
                string[] parts = cookiePair.Trim().Split('=');
                if (parts.Length == 2)
                {
                    string cookieName = parts[0].Trim();
                    string cookieValue = parts[1].Trim();
                    var cookie = new Cookie(cookieName, cookieValue, "/", domain);
                    cookies.Add(cookie);
                }
            }

            return cookies;
        }
        private static string GenerateUserAgent()
        {
            var faker = new Faker();
            return faker.Internet.UserAgent();
        }

    }
}
