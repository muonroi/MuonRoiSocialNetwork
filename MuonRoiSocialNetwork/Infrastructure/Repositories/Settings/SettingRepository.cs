﻿using AutoMapper;
using BaseConfig.BaseDbContext.BaseRepository;
using BaseConfig.Extentions.ImageHelper;
using BaseConfig.Infrashtructure;
using BaseConfig.MethodResult;
using Microsoft.EntityFrameworkCore;
using MuonRoiSocialNetwork.Common.Enums.Settings;
using MuonRoiSocialNetwork.Common.Models.Settings.Response;
using MuonRoiSocialNetwork.Domains.DomainObjects.Settings;
using MuonRoiSocialNetwork.Domains.Interfaces.Commands.Settings;
using Newtonsoft.Json;

namespace MuonRoiSocialNetwork.Infrastructure.Repositories.Settings
{
    /// <summary>
    /// Hanlde function
    /// </summary>
    public class SettingRepository : BaseRepository<SystemSettings>, ISettingRepository
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="authContext"></param>
        public SettingRepository(MuonRoiSocialNetworkDbContext dbContext, AuthContext authContext) : base(dbContext, authContext)
        {
        }

    }
}
