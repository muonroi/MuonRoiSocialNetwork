﻿using BaseConfig.BaseDbContext.BaseRepository;
using BaseConfig.Infrashtructure;
using MuonRoiSocialNetwork.Domains.Interfaces.Repository.StoriesData;
using StoryDataEntity = MuonRoiSocialNetwork.Domains.DomainObjects.Storys.StoriesData;
namespace MuonRoiSocialNetwork.Infrastructure.Repositories.StoriesData
{
    /// <summary>
    /// Define class
    /// </summary>
    public class StoryDataRepository : BaseRepository<StoryDataEntity>, IStoryDataRepository
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="authContext"></param>
        public StoryDataRepository(MuonRoiSocialNetworkDbContext dbContext, AuthContext authContext) : base(dbContext, authContext)
        {
        }
    }
}
