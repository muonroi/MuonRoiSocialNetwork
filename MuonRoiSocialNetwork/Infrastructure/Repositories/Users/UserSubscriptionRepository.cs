﻿using BaseConfig.BaseDbContext;
using BaseConfig.BaseDbContext.BaseRepository;
using BaseConfig.Extentions.Datetime;
using BaseConfig.Infrashtructure;
using Microsoft.EntityFrameworkCore;
using MuonRoiSocialNetwork.Domains.DomainObjects.Users;
using MuonRoiSocialNetwork.Domains.Interfaces.Commands.Users;

namespace MuonRoiSocialNetwork.Infrastructure.Repositories.Users
{
    /// <summary>
    /// Handle reposiotory
    /// </summary>
    public class UserSubscriptionRepository : BaseRepository<UserSubscription>, IUserSubscriptionRepository
    {
        private readonly MuonRoiSocialNetworkDbContext _dbcontext;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="authContext"></param>
        public UserSubscriptionRepository(MuonRoiSocialNetworkDbContext dbContext, AuthContext authContext) : base(dbContext, authContext)
        {
            _dbcontext = dbContext;
        }
        /// <summary>
        /// In active
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task InActiveSubscriptionThirdtyMonth(long input)
        {
            var userSubscriptionDetail = await _queryable.FirstAsync(x => x.Id == input);
            userSubscriptionDetail.IsActive = false;
            userSubscriptionDetail.UpdatedDateTS = DateTime.UtcNow.GetTimeStamp(includedTimeValue: true);
            _dbcontext.UserSubscription?.Update(userSubscriptionDetail);
            await _dbBaseContext.SaveEntitiesAsync();
        }
    }
}
