﻿using Bogus;
using Hangfire;
using HtmlAgilityPack;
using MediatR;
using MuonRoiSocialNetwork.Application.Commands.Chapter;
using MuonRoiSocialNetwork.Common.Settings.Appsettings;
using MuonRoiSocialNetwork.Domains.Interfaces.Queries.Category;
using MuonRoiSocialNetwork.Domains.Interfaces.Repository.StoriesData;
using Npgsql;
using System.Diagnostics;
using System.Net;
using System.Text.RegularExpressions;

namespace MuonRoiSocialNetwork.Infrastructure.Crawl
{
    /// <summary>
    /// Crawl chapter data and check missing chapter 
    /// </summary>
    public class CrawlStoriesOnYY
    {
        private readonly IMediator _mediator;
        private readonly IConfiguration _configuration;
        private readonly IStoryDataRepository _storyDataRepository;
        private readonly ICategoryQueries _categoryQueries;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="mediator"></param>
        /// <param name="configuration"></param>
        /// <param name="storyDataRepository"></param>
        /// <param name="categoryQueries"></param>
        public CrawlStoriesOnYY(IMediator mediator, IConfiguration configuration, IStoryDataRepository storyDataRepository, ICategoryQueries categoryQueries)
        {
            _mediator = mediator;
            _configuration = configuration;
            _storyDataRepository = storyDataRepository;
            _categoryQueries = categoryQueries;
        }
        /// <summary>
        /// Check daily chapter (missing)
        /// </summary>
        /// <returns></returns>
        [JobDisplayName("missing-duplicate-crawl")]
        [DisableConcurrentExecution(0)]
        public async Task CheckDailyChapter()
        {
            var listCategory = await _categoryQueries.GetAllCategory();
            var category = listCategory.Result?.Items?.Select(x => x.Id).ToList();
            foreach (var item in category)
            {
                var storyDataInfo = await _storyDataRepository.GetWhereAsync(x => x.CategoryId == item);
                foreach (var story in storyDataInfo.ToList())
                {
                    var stopwatch = new Stopwatch();
                    stopwatch.Start();
                    await CheckConsecutiveNumberOfChapter(story.Url, story.StoryId, story.MaxChapter);
                    await RemoveDuplicateChapter((int)story.StoryId);
                    stopwatch.Stop();
                    Console.WriteLine($"COMPLETE:{story.Url} - {(int)story.StoryId} chapter - crawl in {stopwatch.ElapsedMilliseconds} ms ");
                    stopwatch.Reset();
                }
            }
        }
        /// <summary>
        /// Remove duplicate chapter
        /// </summary>
        /// <param name="storyId"></param>
        /// <returns></returns>
        private async Task RemoveDuplicateChapter(int storyId)
        {
            string connectionString = _configuration[ConstAppSettings.Instance.CONNECTIONSTRING_DB] ?? ConstAppSettings.Instance.CONNECTIONSTRING_DB;
            using var connection = new NpgsqlConnection(connectionString);
            connection.Open();
            string removeDuplicate = $@"DELETE FROM chapter
                                        WHERE (story_id, number_of_chapter) IN (
                                            SELECT story_id, number_of_chapter
                                            FROM chapter
                                            WHERE story_id = {storyId}
                                            GROUP BY story_id, number_of_chapter
                                            HAVING COUNT(*) > 1
                                        )
                                        AND ctid NOT IN (
                                            SELECT MIN(ctid) AS min_ctid
                                            FROM chapter
                                            WHERE story_id = {storyId}
                                            GROUP BY number_of_chapter
                                            HAVING COUNT(*) > 1
                                        );
";
            using (var command = new NpgsqlCommand(removeDuplicate, connection))
            {
                var result = await command.ExecuteNonQueryAsync();
                Console.WriteLine($"remove {result} duplicate chapter from {storyId}");
            }

            connection.Close();
        }
        public static CookieContainer ParseCookies(string cookieString, string domain)
        {
            var cookies = new CookieContainer();

            string[] cookiePairs = cookieString.Split(';');

            foreach (string cookiePair in cookiePairs)
            {
                string[] parts = cookiePair.Trim().Split('=');
                if (parts.Length == 2)
                {
                    string cookieName = parts[0].Trim();
                    string cookieValue = parts[1].Trim();
                    var cookie = new Cookie(cookieName, cookieValue, "/", domain);
                    cookies.Add(cookie);
                }
            }

            return cookies;
        }
        //public async Task CrawlChapterManual(long storyId, int totalChapter, string urls)
        //{
        //    var detectNameStory = new Regex("\\/truyen\\/([^\\/]+)\\/");
        //    var storyName = detectNameStory.Match(urls).Groups[1].Value;
        //    string connectionString = _configuration[ConstAppSettings.Instance.CONNECTIONSTRING_DB] ?? ConstAppSettings.Instance.CONNECTIONSTRING_DB;
        //    for (var i = 1; i <= totalChapter; i++)
        //    {
        //        try
        //        {
        //            string cookieString = "cf_clearance=fwVoIQIQUIw97LwZkzLzu2KmkkubZ2JSS4RJVAg7OxU-1697890814-0-1-c4d1f544.6a7b9b77.42fade56-250.0.0; messages=W1siX19qc29uX21lc3NhZ2UiLDAsMjUsIlN1Y2Nlc3NmdWxseSBzaWduZWQgaW4gYXMgTXVvbnJvaWlpLiJdXQ:1qx7Gy:cACuDhQnhWiRsveD45zx_HWXRNyGJe0yQUBOdAQZMws; csrftoken=sOaq0NOCEVSjF6PvZPdg8Map0m3d5RBfAVpQ6AWW2phg5KkfDDA7IdWnbgivIC2d; truyenyyid=8weu7kkvlpgggi11t3onz8vsfvzz09it";
        //            string domain = "truyenyy.pro";
        //            CookieContainer cookies = ParseCookies(cookieString, domain);
        //            WebClient webClient = null;
        //            var userAgent = GenerateUserAgent();
        //            while (webClient == null)
        //            {
        //                Console.WriteLine($"{storyName} => trying...");
        //                webClient = CloudflareEvader.CreateBypassedWebClient(userAgent, url);
        //            }
        //            webClient.Headers.Add(HttpRequestHeader.UserAgent, userAgent);
        //            webClient.Headers.Add(HttpRequestHeader.Cookie, cookieString);
        //            string content = await webClient.DownloadStringTaskAsync(url);
        //            var document = new HtmlDocument();
        //            document.LoadHtml(content);
        //            var chapterNumberRaw = document?.DocumentNode.Descendants("h1").Where(d => d.Attributes["class"].Value.Contains("chap-title")).FirstOrDefault();
        //            var title = document?.DocumentNode.SelectSingleNode(".//div[@class='chapter']/div/h2");
        //            var contents = document?.DocumentNode.SelectSingleNode("//*[@id=\"inner_chap_content_1\"]");
        //            if (contents is null)
        //            {
        //                Console.WriteLine($"{storyName} => No content found - {storyId}");
        //                return;
        //            }
        //            var chapterNumber = chapterNumberRaw != null ? Regex.Match(chapterNumberRaw.SelectSingleNode("span").InnerText, @"\d+").Value : "-1";
        //            var chapter = new CrawlChapterDto
        //            {
        //                NumberOfChapter = int.Parse(chapterNumber),
        //                ChapterTitle = title is null || string.IsNullOrEmpty(title.InnerText.Trim()) ? "Không đề" : title.InnerText.Trim(),
        //                Body = contents is null || string.IsNullOrEmpty(contents.InnerHtml.Trim()) ? "Chưa cập nhật nội dung" : contents.InnerHtml.Trim(),
        //                StoryId = (int)storyId
        //            };
        //            var chapterResult = _mapper.Map<CreateChapterCommand>(chapter);
        //            if (chapterResult.Body.Length >= 750)
        //            {
        //                await _mediator.Send(chapterResult).ConfigureAwait(false);
        //                Console.WriteLine($"{storyName} => Chap {chapterNumber} [{title?.InnerText.Trim()}] - {storyId}");
        //            }
        //            await PostChapter(new List<Chapter> { chapter });

        //            Console.WriteLine($"{storyName} => Chap {chapterNumber} [{title?.InnerText.Trim()}] - {storyId}");
        //        }
        //        catch (Exception ex)
        //        {
        //            Console.WriteLine(ex.Message);
        //        }
        //    }
        //}

        /// <summary>
        /// Check chapter missing
        /// </summary>
        /// <param name="urls"></param>
        /// <param name="storyId"></param>
        /// <param name="totalChapter"></param>
        /// <returns></returns>
        private async Task CheckConsecutiveNumberOfChapter(string urls, long storyId, int totalChapter)
        {
            var detectNameStory = new Regex("\\/truyen\\/([^\\/]+)\\/");
            var storyName = detectNameStory.Match(urls).Groups[1].Value;
            string connectionString = _configuration[ConstAppSettings.Instance.CONNECTIONSTRING_DB] ?? ConstAppSettings.Instance.CONNECTIONSTRING_DB;
            var baseUrl = "https://truyenyy.pro/truyen/{0}/chuong-{1}.html";
            using var connection = new NpgsqlConnection(connectionString);
            connection.Open();
            string checkConsecutiveChapter = $@"
                WITH ChapterRange AS (
                SELECT story_id, generate_series(1, {totalChapter}) AS all_chapters
                FROM chapter c 
                WHERE c.story_id = {storyId}
                GROUP BY c.story_id)
                SELECT  all_chapters AS missing_chapter
                FROM ChapterRange 
                WHERE all_chapters NOT IN (SELECT c.number_of_chapter FROM chapter c WHERE c.story_id = {storyId});";
            using (var command = new NpgsqlCommand(checkConsecutiveChapter, connection))
            {
                using var reader = await command.ExecuteReaderAsync();
                var chapterNumbers = new List<int>();
                while (reader.Read())
                {
                    int chapterNumber = reader.GetInt32(0);
                    chapterNumbers.Add(chapterNumber);
                }

                foreach (int singleChapter in chapterNumbers)
                {
                    var url = string.Format(baseUrl, storyName, singleChapter);
                    try
                    {
                        using HttpClient client = new();
                        var response = await client.GetAsync(url);
                        if (response.IsSuccessStatusCode)
                        {
                            var content = await response.Content.ReadAsStringAsync();
                            var document = new HtmlDocument();
                            document.LoadHtml(content);
                            var chapterNumberRaw = document?.DocumentNode.Descendants("h1").Where(d => d.Attributes["class"].Value.Contains("chap-title")).FirstOrDefault();
                            var title = document?.DocumentNode.SelectSingleNode(".//div[@class='chapter']/div/h2");
                            var contents = document?.DocumentNode.SelectSingleNode("//*[@id=\"inner_chap_content_1\"]");
                            if (contents is null)
                            {
                                Console.WriteLine($"{storyName} => No content found - {storyId}");
                                return;
                            }
                            var chapterNumber = chapterNumberRaw != null ? Regex.Match(chapterNumberRaw.SelectSingleNode("span").InnerText, @"\d+").Value : "-1";
                            var chapterResult = new CreateChapterCommand
                            {
                                NumberOfChapter = int.Parse(chapterNumber),
                                ChapterTitle = title is null || string.IsNullOrEmpty(title.InnerText.Trim()) ? "Không đề" : title.InnerText.Trim(),
                                Body = contents is null || string.IsNullOrEmpty(contents.InnerHtml.Trim()) ? "Chưa cập nhật nội dung" : contents.InnerHtml.Trim(),
                                StoryId = (int)storyId
                            };

                            if (chapterResult.Body.Length >= 750)
                            {
                                await _mediator.Send(chapterResult).ConfigureAwait(false);
                                Console.WriteLine($"{storyName} => Chap {chapterNumber} [{title?.InnerText.Trim()}] - {storyId}");
                            }
                        }


                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            connection.Close();
        }
        private static string GenerateUserAgent()
        {
            var faker = new Faker();
            return faker.Internet.UserAgent();
        }
    }
}
