﻿using AutoMapper;
using BaseConfig.BaseDbContext.BaseQuery;
using BaseConfig.Extentions.ImageHelper;
using BaseConfig.Infrashtructure;
using BaseConfig.MethodResult;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using MuonRoiSocialNetwork.Common.Enums.Settings;
using MuonRoiSocialNetwork.Common.Models.Settings.Response;
using MuonRoiSocialNetwork.Domains.DomainObjects.Settings;
using MuonRoiSocialNetwork.Domains.Interfaces.Queries.Settings;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace MuonRoiSocialNetwork.Infrastructure.Queries.Settings
{
    /// <summary>
    /// Handle function
    /// </summary>
    public class SettingQueries : BaseQuery<SystemSettings>, ISettingQueries
    {
        private readonly IConfiguration _configuration;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="authContext"></param>
        /// <param name="cache"></param>
        /// <param name="mapper"></param>
        /// <param name="configuration"></param>
        public SettingQueries(MuonRoiSocialNetworkDbContext dbContext, AuthContext authContext, IDistributedCache cache, IMapper mapper, IConfiguration configuration) : base(dbContext, authContext, cache, mapper)
        {
            _configuration = configuration;
        }
        /// <summary>
        /// Get url banner
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public async Task<MethodResult<SettingBannerResponse>> GetBannerUrlAsync(EnumSettingType type)
        {
            var methodResult = new MethodResult<SettingBannerResponse>();
            var bannerList = _queryable.Where(x => x.Type == type).Select(x => x);
            var result = await bannerList.AnyAsync();
            if (!result)
            {
                methodResult.StatusCode = StatusCodes.Status404NotFound;
                return methodResult;
            }
            methodResult.Result = _mapper.Map<SettingBannerResponse>(bannerList.FirstOrDefault());
            var tempUrlImagesUpdate = JsonConvert.DeserializeObject<List<string>>(bannerList.First().Content) ?? new List<string>();
            methodResult.Result.BannerUrl = tempUrlImagesUpdate.Select(x => ImageHelper.GetUrl(_configuration, x));
            return methodResult;
        }
        /// <summary>
        /// Get all setting or single
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public async Task<MethodResult<List<SettingResponse>>> GetSettingsAsync(EnumSettingType type)
        {
            var methodResult = new MethodResult<List<SettingResponse>>();
            var settings = !(type == EnumSettingType.ALL) ? _queryable.Where(x => x.Type == type).Select(x => x) : _queryable.Select(x => x);
            var result = await settings.AnyAsync();
            if (!result)
            {
                return methodResult;
            }
            methodResult.Result = _mapper.Map<List<SettingResponse>>(settings);
            return methodResult;
        }
    }
}
