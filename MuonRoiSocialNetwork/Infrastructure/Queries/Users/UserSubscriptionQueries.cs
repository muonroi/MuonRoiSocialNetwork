﻿using AutoMapper;
using BaseConfig.BaseDbContext;
using BaseConfig.BaseDbContext.BaseQuery;
using BaseConfig.Infrashtructure;
using Microsoft.Extensions.Caching.Distributed;
using MuonRoiSocialNetwork.Domains.DomainObjects.Users;
using MuonRoiSocialNetwork.Domains.Interfaces.Queries.Users;

namespace MuonRoiSocialNetwork.Infrastructure.Queries.Users
{
    /// <summary>
    /// Handle class
    /// </summary>
    public class UserSubscriptionQueries : BaseQuery<UserSubscription>, IUserSubscriptionQueries
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="authContext"></param>
        /// <param name="cache"></param>
        /// <param name="mapper"></param>
        public UserSubscriptionQueries(MuonRoiSocialNetworkDbContext dbContext, AuthContext authContext, IDistributedCache cache, IMapper mapper) : base(dbContext, authContext, cache, mapper)
        {
        }
    }
}
