﻿using AutoMapper;
using BaseConfig.BaseDbContext.BaseQuery;
using BaseConfig.BaseDbContext.Common;
using BaseConfig.Extentions.ImageHelper;
using BaseConfig.Infrashtructure;
using BaseConfig.MethodResult;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using MuonRoiSocialNetwork.Common.Enums.Storys;
using MuonRoiSocialNetwork.Common.Models.Stories.Response;
using MuonRoiSocialNetwork.Domains.DomainObjects.Storys;
using MuonRoiSocialNetwork.Domains.Interfaces.Commands.Stories;
using MuonRoiSocialNetwork.Domains.Interfaces.Commands.Users;
using MuonRoiSocialNetwork.Domains.Interfaces.Queries.Stories;

namespace MuonRoiSocialNetwork.Infrastructure.Queries.Stories
{
    /// <summary>
    /// Handler
    /// </summary>
    public class StoriesForUserQueries : BaseQuery<StoriesForUser>, IStoriesForUserQueries
    {
        private readonly IUserRepository _userRepository;
        private readonly IStoriesRepository _storiesRepository;
        private readonly IConfiguration _configuration;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="authContext"></param>
        /// <param name="cache"></param>
        /// <param name="mapper"></param>
        /// <param name="userRepository"></param>
        /// <param name="storiesRepository"></param>
        /// <param name="configuration"></param>
        public StoriesForUserQueries(MuonRoiSocialNetworkDbContext dbContext, AuthContext authContext, IDistributedCache cache, IMapper mapper, IUserRepository userRepository, IStoriesRepository storiesRepository, IConfiguration configuration) : base(dbContext, authContext, cache, mapper)
        {
            _userRepository = userRepository;
            _storiesRepository = storiesRepository;
            _configuration = configuration;
        }
        /// <summary>
        /// Get stories for user
        /// </summary>
        /// <param name="type"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<MethodResult<PagingItemsDTO<StoryModelResponse>>> GetStoriesForUser(EnumStoriesForUserType type, int pageIndex, int pageSize)
        {
            var methodResult = new MethodResult<PagingItemsDTO<StoryModelResponse>>();
            #region Check user is exist
            var isUserExist = await _userRepository.ExistUserByGuidAsync(Guid.Parse(_authContext.CurrentUserId));
            if (!isUserExist.IsOK)
            {
                methodResult.StatusCode = StatusCodes.Status400BadRequest;
                return methodResult;
            }
            #endregion

            #region Get stories for user by type
            var storiesForUser = _queryable
                .AsNoTracking()
                .Where(x => x.UserGuid == Guid.Parse(_authContext.CurrentUserId) && x.StoriesForUserType == type)
                .Select(x => x);

            if (storiesForUser == null)
            {
                methodResult.Result = new PagingItemsDTO<StoryModelResponse>
                {
                    Items = new List<StoryModelResponse>(),
                    PagingInfo = default
                };
                methodResult.StatusCode = StatusCodes.Status200OK;
                return methodResult;
            }
            #endregion

            #region Get stories by stories by user
            var storiesForUserPaging = await GetListPaging(storiesForUser, pageIndex, pageSize).ConfigureAwait(false);
            if (storiesForUserPaging.Items is null)
            {
                methodResult.Result = new PagingItemsDTO<StoryModelResponse>
                {
                    Items = new List<StoryModelResponse>(),
                    PagingInfo = storiesForUserPaging.PagingInfo
                };
                methodResult.StatusCode = StatusCodes.Status200OK;
                return methodResult;
            }
            var storiesResponse = new List<StoryModelResponse>();
            foreach (var item in storiesForUserPaging.Items.OrderByDescending(x => x.UpdatedDateTS))
            {
                var tempStory = await _storiesRepository.GetByIdAsync(item.StoryId);
                var result = _mapper.Map<StoryModelResponse>(tempStory);
                result.CurrentIndex = item.ChapterIndex;
                result.PageCurrentIndex = item.ChapterPageIndex;
                result.NumberOfChapter = item.ChapterNumber;
                result.IdForUser = item.Id;
                result.ChapterLatestLocation = item.ChapterLatestLocation;
                result.ChapterlatestId = item.ChapterId;
                storiesResponse.Add(result);
            }
            var tempStoryList = storiesResponse.ToList();
            for (int i = 0; i < tempStoryList.Count; i++)
            {
                tempStoryList[i].ImgUrl = ImageHelper.GetUrl(_configuration, tempStoryList[i].ImgUrl);
            }
            methodResult.Result = new PagingItemsDTO<StoryModelResponse>
            {
                Items = tempStoryList.DistinctBy(x => x.Id).ToList(),
                PagingInfo = storiesForUserPaging.PagingInfo
            };
            methodResult.StatusCode = StatusCodes.Status200OK;
            return methodResult;
            #endregion

        }
    }
}
