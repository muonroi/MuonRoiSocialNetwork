﻿using AutoMapper;
using BaseConfig.BaseDbContext.BaseQuery;
using BaseConfig.BaseDbContext.Common;
using BaseConfig.Infrashtructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using MuonRoiSocialNetwork.Common.Models.Stories.Dtos;
using MuonRoiSocialNetwork.Common.Models.Stories.Response;
using MuonRoiSocialNetwork.Domains.Interfaces.Queries.StoriesData;
using StoryDataEntity = MuonRoiSocialNetwork.Domains.DomainObjects.Storys.StoriesData;
namespace MuonRoiSocialNetwork.Infrastructure.Queries.StoriesData
{
    /// <summary>
    /// Define class
    /// </summary>
    public class StoryDataQueries : BaseQuery<StoryDataEntity>, IStoryDataQueries
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="authContext"></param>
        /// <param name="cache"></param>
        /// <param name="mapper"></param>
        public StoryDataQueries(MuonRoiSocialNetworkDbContext dbContext, AuthContext authContext, IDistributedCache cache, IMapper mapper) : base(dbContext, authContext, cache, mapper)
        {
        }
        /// <summary>
        /// Get story data by category id
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<PagingItemsDTO<StoryDataDto>> GetStoryDataByCategory(int categoryId, int pageIndex, int pageSize)
        {
            var querySearch = _queryable.AsNoTracking().Where(x => x.CategoryId == categoryId).Select(x => x);
            var pagingStoryItemsDTO = await GetListPaging(querySearch, pageIndex, pageSize).ConfigureAwait(false);
            var result = _mapper.Map<IEnumerable<StoryDataDto>>(pagingStoryItemsDTO.Items);
            return new PagingItemsDTO<StoryDataDto>
            {
                Items = result,
                PagingInfo = pagingStoryItemsDTO.PagingInfo
            };
        }
    }
}
