﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MuonRoiSocialNetwork.Domains.DomainObjects.Settings;

namespace MuonRoiSocialNetwork.Infrastructure.EFConfigs.Settings
{
    /// <summary>
    /// Settings table configuration
    /// </summary>
    public class SystemSettingsConfiguration : IEntityTypeConfiguration<SystemSettings>
    {
        /// <summary>
        /// Config detail
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(EntityTypeBuilder<SystemSettings> builder)
        {
            builder.ToTable(nameof(SystemSettings).ToLower());
            builder.HasKey(x => x.Id);
        }
    }
}
