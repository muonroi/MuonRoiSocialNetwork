﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using MuonRoi.Social_Network.Storys;
using MuonRoiSocialNetwork.Domains.DomainObjects.Storys;

namespace MuonRoiSocialNetwork.Infrastructure.EFConfigs.Storys
{
    /// <summary>
    /// Config table story recent
    /// </summary>
    public class StoryRecentConfiguration : IEntityTypeConfiguration<StoriesForUser>
    {
        /// <summary>
        /// Configuration StoryPublish
        /// </summary>
        public void Configure(EntityTypeBuilder<StoriesForUser> builder)
        {
            builder.ToTable(nameof(StoriesForUser).ToLower());
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseIdentityColumn();
            builder.HasOne(x => x.UserMember).WithMany(x => x.StoryRecent).HasForeignKey(x => x.UserGuid);
            builder.HasOne(x => x.Story).WithMany(x => x.StoryRecent).HasForeignKey(x => x.StoryId);
        }
    }
}
