﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using MuonRoiSocialNetwork.Domains.DomainObjects.Storys;

namespace MuonRoiSocialNetwork.Infrastructure.EFConfigs.Storys
{
    /// <summary>
    /// Stories data configuration
    /// </summary>
    public class StoriesDataConfiguration : IEntityTypeConfiguration<StoriesData>
    {
        /// <summary>
        /// Configuration Story data
        /// </summary>
        public void Configure(EntityTypeBuilder<StoriesData> builder)
        {
            builder.ToTable(nameof(StoriesData).ToLower());
            builder.HasKey(x => x.Id);
            builder.HasOne(x => x.Category)
                .WithMany(x => x.StoriesDatas)
                .HasForeignKey(x => x.CategoryId);
        }
    }
}
