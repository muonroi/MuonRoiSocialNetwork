﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using MuonRoiSocialNetwork.Domains.DomainObjects.Users;

namespace MuonRoiSocialNetwork.Infrastructure.EFConfigs.Users
{
    /// <summary>
    /// Config table usersubscription
    /// </summary>
    public class UserSubscriptionConfiguration : IEntityTypeConfiguration<UserSubscription>
    {
        /// <summary>
        /// Configuration UserSubscription
        /// </summary>
        public void Configure(EntityTypeBuilder<UserSubscription> builder)
        {
            builder.ToTable(nameof(UserSubscription).ToLower());
            builder.HasKey(t => t.Id);

        }
    }
}
