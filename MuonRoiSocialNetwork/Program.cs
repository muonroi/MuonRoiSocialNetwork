using MuonRoiSocialNetwork.StartupConfig;

await WebApplication.CreateBuilder(args)
    .RegisterServices()
    .Build()
    .CustomMidleware()
    .RunAsync();
