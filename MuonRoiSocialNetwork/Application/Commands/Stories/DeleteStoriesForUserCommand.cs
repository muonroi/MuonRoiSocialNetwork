﻿using AutoMapper;
using BaseConfig.EntityObject.Entity;
using BaseConfig.Exeptions;
using BaseConfig.Infrashtructure;
using BaseConfig.MethodResult;
using MediatR;
using MuonRoi.Social_Network.Storys;
using MuonRoi.Social_Network.Users;
using MuonRoiSocialNetwork.Application.Commands.Base.Stories;
using MuonRoiSocialNetwork.Domains.Interfaces.Commands.Stories;
using MuonRoiSocialNetwork.Domains.Interfaces.Queries.Stories;
using MuonRoiSocialNetwork.Domains.Interfaces.Queries.Users;
using Serilog;

namespace MuonRoiSocialNetwork.Application.Commands.Stories
{
    /// <summary>
    /// Request delete story recent
    /// </summary>
    public class DeleteStoriesForUserCommand : IRequest<MethodResult<bool>>
    {
        /// <summary>
        /// Story id recent
        /// </summary>
        public long Id { get; set; }
    }
    /// <summary>
    /// Handler
    /// </summary>
    public class DeleteStoriesForUserCommandHandler : BaseStoriesCommandHandler, IRequestHandler<DeleteStoriesForUserCommand, MethodResult<bool>>
    {
        private readonly ILogger<DeleteStoriesForUserCommandHandler> _logger;
        private readonly AuthContext _authContext;
        private readonly IUserQueries _userQueries;
        private readonly IStoriesForUserRepository _storiesForUserRepository;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="configuration"></param>
        /// <param name="storiesQuerie"></param>
        /// <param name="storiesRepository"></param>
        /// <param name="logger"></param>
        /// <param name="authContext"></param>
        /// <param name="userQueries"></param>
        /// <param name="storiesForUserRepository"></param>
        public DeleteStoriesForUserCommandHandler(IMapper mapper, IConfiguration configuration, IStoriesQueries storiesQuerie, IStoriesRepository storiesRepository, ILoggerFactory logger, AuthContext authContext, IUserQueries userQueries, IStoriesForUserRepository storiesForUserRepository) : base(mapper, configuration, storiesQuerie, storiesRepository)
        {
            _logger = logger.CreateLogger<DeleteStoriesForUserCommandHandler>();
            _authContext = authContext;
            _userQueries = userQueries;
            _storiesForUserRepository = storiesForUserRepository;
        }
        /// <summary>
        /// Function handler
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<MethodResult<bool>> Handle(DeleteStoriesForUserCommand request, CancellationToken cancellationToken)
        {
            var methodResult = new MethodResult<bool>();
            try
            {
                #region Valid request
                if (request is null)
                {
                    methodResult.StatusCode = StatusCodes.Status400BadRequest;
                    methodResult.AddApiErrorMessage(
                        nameof(EnumUserErrorCodes.USRC49C),
                        new[] { Helpers.GenerateErrorResult(nameof(EnumUserErrorCodes.USRC49C), EnumUserErrorCodes.USRC49C) }
                    );
                    return methodResult;
                }
                #endregion

                #region Check user is exist
                var baseUserResponse = await _userQueries.GetUserModelByGuidAsync(new Guid(_authContext.CurrentUserId));
                if (baseUserResponse.Result is null)
                {
                    methodResult.StatusCode = StatusCodes.Status400BadRequest;
                    methodResult.AddApiErrorMessage(
                        nameof(EnumUserErrorCodes.USR13C),
                        new[] { Helpers.GenerateErrorResult(nameof(_authContext.CurrentUsername), _authContext.CurrentUsername ?? "") }
                    );
                    return methodResult;
                }
                #endregion

                #region Check is exist story for user
                var existStoryForUser = await _storiesForUserRepository.GetWhereFirstOrDefaultAsync(x => x.StoryId == request.Id);
                var findStoriesForUserById = await _storiesForUserRepository.GetByIdAsync(request.Id);
                if (existStoryForUser is null && findStoriesForUserById is null)
                {
                    methodResult.Result = false;
                    methodResult.StatusCode = StatusCodes.Status400BadRequest;
                    methodResult.AddApiErrorMessage(
                        nameof(EnumStoryErrorCode.ST10),
                        new[] { Helpers.GenerateErrorResult(nameof(_authContext.CurrentUsername), _authContext.CurrentUsername ?? "") }
                    );
                    return methodResult;
                }
                #endregion

                #region Delete story for user
                await _storiesForUserRepository.DeleteAsync(existStoryForUser ?? findStoriesForUserById);
                await _storiesRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
                #endregion
                methodResult.Result = true;
                return methodResult;
            }
            catch (CustomException ex)
            {
                Log.Error(ex.Message, ex);
                methodResult.StatusCode = StatusCodes.Status400BadRequest;
                _logger.LogError($" -->(Delete story for user) STEP CUSTOMEXCEPTION --> {ex} ---->");
                methodResult.AddResultFromErrorList(ex.ErrorMessages);
                methodResult.Result = false;
                return methodResult;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                methodResult.StatusCode = StatusCodes.Status400BadRequest;
                _logger.LogError($" -->(Delete story for user) STEP EXEPTION MESSAGE -->{ex} ---->");
                _logger.LogError($" -->(Delete story for user) STEP EXEPTION STACK -->{ex.StackTrace} ---->");
                methodResult.AddErrorMessage(Helpers.GetExceptionMessage(ex), ex.StackTrace ?? "");
                methodResult.Result = false;
                return methodResult;
            }
        }
    }
}
