﻿using AutoMapper;
using BaseConfig.EntityObject.Entity;
using BaseConfig.Exeptions;
using BaseConfig.MethodResult;
using MediatR;
using MuonRoi.Social_Network.Users;
using MuonRoiSocialNetwork.Application.Commands.Base.Stories;
using MuonRoiSocialNetwork.Domains.Interfaces.Commands.Stories;
using MuonRoiSocialNetwork.Domains.Interfaces.Queries.Stories;
using Newtonsoft.Json;
using Serilog;

namespace MuonRoiSocialNetwork.Application.Commands.Stories
{
    /// <summary>
    /// Request
    /// </summary>
    public class DeleteBookmarkStoryCommand : IRequest<MethodResult<bool>>
    {
        /// <summary>
        /// story id
        /// </summary>
        [JsonProperty("bookmark-id")]
        public long BookmarkId { get; set; }
    }
    /// <summary>
    /// Handler
    /// </summary>
    public class DeleteBookmarkStoryCommandHandler : BaseStoriesCommandHandler, IRequestHandler<DeleteBookmarkStoryCommand, MethodResult<bool>>
    {
        private readonly ILogger<BookmarkStoryCommandHandler> _logger;
        private readonly IStoriesFavoriteRepository _storiesFavoriteRepository;
        private readonly IStoriesFavoriteQueries _storiesFavoriteQueries;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="configuration"></param>
        /// <param name="storiesQuerie"></param>
        /// <param name="storiesRepository"></param>
        /// <param name="logger"></param>
        /// <param name="storiesFavoriteRepository"></param>
        /// <param name="storiesFavoriteQueries"></param>
        public DeleteBookmarkStoryCommandHandler(IMapper mapper, IConfiguration configuration, IStoriesQueries storiesQuerie, IStoriesRepository storiesRepository, ILoggerFactory logger, IStoriesFavoriteRepository storiesFavoriteRepository, IStoriesFavoriteQueries storiesFavoriteQueries) : base(mapper, configuration, storiesQuerie, storiesRepository)
        {
            _logger = logger.CreateLogger<BookmarkStoryCommandHandler>();
            _storiesFavoriteRepository = storiesFavoriteRepository;
            _storiesFavoriteQueries = storiesFavoriteQueries;
        }
        /// <summary>
        /// Handle funciton
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<MethodResult<bool>> Handle(DeleteBookmarkStoryCommand request, CancellationToken cancellationToken)
        {
            var methodResult = new MethodResult<bool>();
            try
            {
                #region Valid request
                if (request is null)
                {
                    methodResult.StatusCode = StatusCodes.Status400BadRequest;
                    methodResult.AddApiErrorMessage(
                        nameof(EnumUserErrorCodes.USRC49C),
                        new[] { Helpers.GenerateErrorResult(nameof(EnumUserErrorCodes.USRC49C), EnumUserErrorCodes.USRC49C) }
                    );
                    return methodResult;
                }
                #endregion

                #region Check story bookmark for user
                var bookmarkResult = await _storiesFavoriteQueries.GetByIdAsync(request.BookmarkId);
                if (bookmarkResult is null)
                {
                    methodResult.Result = false;
                    methodResult.StatusCode = StatusCodes.Status400BadRequest;
                    return methodResult;
                }
                #endregion
                await _storiesFavoriteRepository.DeleteAsync(bookmarkResult);
                await _storiesFavoriteRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken).ConfigureAwait(false);
                methodResult.Result = true;
                methodResult.StatusCode = StatusCodes.Status200OK;
                return methodResult;
            }
            catch (CustomException ex)
            {
                Log.Error(ex.Message, ex);
                methodResult.StatusCode = StatusCodes.Status400BadRequest;
                _logger.LogError($" -->(Bookmark story) STEP CUSTOMEXCEPTION --> {ex} ---->");
                methodResult.AddResultFromErrorList(ex.ErrorMessages);
                methodResult.Result = false;
                return methodResult;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                methodResult.StatusCode = StatusCodes.Status400BadRequest;
                _logger.LogError($" -->(Bookmark story) STEP EXEPTION MESSAGE -->{ex} ---->");
                _logger.LogError($" -->(Bookmark story) STEP EXEPTION STACK -->{ex.StackTrace} ---->");
                methodResult.AddErrorMessage(Helpers.GetExceptionMessage(ex), ex.StackTrace ?? "");
                methodResult.Result = false;
                return methodResult;
            }
        }
    }
}
