﻿using AutoMapper;
using BaseConfig.EntityObject.Entity;
using BaseConfig.Exeptions;
using BaseConfig.Infrashtructure;
using BaseConfig.MethodResult;
using MediatR;
using Microsoft.AspNetCore.SignalR;
using MuonRoi.Social_Network.Storys;
using MuonRoi.Social_Network.Users;
using MuonRoiSocialNetwork.Application.Commands.Base.Stories;
using MuonRoiSocialNetwork.Common.Models.Notifications.Base;
using MuonRoiSocialNetwork.Common.Models.Stories.Request;
using MuonRoiSocialNetwork.Common.Models.Users.Base.Response;
using MuonRoiSocialNetwork.Common.Settings.SignalRSettings.Enum;
using MuonRoiSocialNetwork.Common.Settings.SignalRSettings.SignleName;
using MuonRoiSocialNetwork.Domains.DomainObjects.Storys;
using MuonRoiSocialNetwork.Domains.Interfaces.Commands.Stories;
using MuonRoiSocialNetwork.Domains.Interfaces.Queries.Stories;
using MuonRoiSocialNetwork.Domains.Interfaces.Queries.Users;
using MuonRoiSocialNetwork.Infrastructure.HubCentral;
using Newtonsoft.Json;
using Serilog;

namespace MuonRoiSocialNetwork.Application.Commands.Stories
{
    /// <summary>
    /// Request
    /// </summary>
    public class BookmarkStoryCommand : BookmarkStoryModelRequest, IRequest<MethodResult<bool>>
    { }
    /// <summary>
    /// Handle
    /// </summary>
    public class BookmarkStoryCommandHandler : BaseStoriesCommandHandler, IRequestHandler<BookmarkStoryCommand, MethodResult<bool>>
    {
        private readonly ILogger<BookmarkStoryCommandHandler> _logger;
        private readonly IUserQueries _userQueries;
        private readonly AuthContext _authContext;
        private readonly IStoriesFavoriteRepository _storiesFavoriteRepository;
        private readonly IHubContext<NotificationHub> _hubContext;
        private readonly IStoryNotificationRepository _storyNotificationRepository;
        private readonly IMediator _mediator;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="configuration"></param>
        /// <param name="storiesQuerie"></param>
        /// <param name="storiesRepository"></param>
        /// <param name="logger"></param>
        /// <param name="userQueries"></param>
        /// <param name="authContext"></param>
        /// <param name="storiesFavoriteRepository"></param>
        /// <param name="hubContext"></param>
        /// <param name="storyNotificationRepository"></param>
        /// <param name="mediator"></param>

        public BookmarkStoryCommandHandler(IMapper mapper, IConfiguration configuration, IStoriesQueries storiesQuerie, IStoriesRepository storiesRepository, ILoggerFactory logger, IUserQueries userQueries, AuthContext authContext, IStoriesFavoriteRepository storiesFavoriteRepository, IHubContext<NotificationHub> hubContext, IStoryNotificationRepository storyNotificationRepository, IMediator mediator) : base(mapper, configuration, storiesQuerie, storiesRepository)
        {
            _logger = logger.CreateLogger<BookmarkStoryCommandHandler>();
            _userQueries = userQueries;
            _authContext = authContext;
            _storiesFavoriteRepository = storiesFavoriteRepository;
            _hubContext = hubContext;
            _storyNotificationRepository = storyNotificationRepository;
            _mediator = mediator;
        }
        /// <summary>
        /// Method handle
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<MethodResult<bool>> Handle(BookmarkStoryCommand request, CancellationToken cancellationToken)
        {
            MethodResult<bool> methodResult = new();
            try
            {
                #region Valid request
                if (request is null)
                {
                    methodResult.StatusCode = StatusCodes.Status400BadRequest;
                    methodResult.AddApiErrorMessage(
                        nameof(EnumUserErrorCodes.USRC49C),
                        new[] { Helpers.GenerateErrorResult(nameof(EnumUserErrorCodes.USRC49C), EnumUserErrorCodes.USRC49C) }
                    );
                    return methodResult;
                }
                #endregion

                #region Check story is exist
                Story existStory = await _storiesQueries.GetByIdAsync(request.StoryId);
                if (existStory is null)
                {
                    methodResult.StatusCode = StatusCodes.Status400BadRequest;
                    methodResult.AddApiErrorMessage(
                        nameof(EnumStoryErrorCode.ST10),
                        new[] { Helpers.GenerateErrorResult(nameof(request.StoryId), request.StoryId.ToString() ?? "") }
                    );
                    return methodResult;
                }
                #endregion

                #region Check user is exist
                MethodResult<BaseUserResponse> baseUserResponse = await _userQueries.GetUserModelByGuidAsync(new Guid(_authContext.CurrentUserId));
                if (baseUserResponse.Result is null)
                {
                    methodResult.StatusCode = StatusCodes.Status400BadRequest;
                    methodResult.AddApiErrorMessage(
                        nameof(EnumUserErrorCodes.USR13C),
                        new[] { Helpers.GenerateErrorResult(nameof(_authContext.CurrentUsername), _authContext.CurrentUsername ?? "") }
                    );
                    return methodResult;
                }
                #endregion

                #region Check user was marked story bookmark?
                var bookmarkResult = await _storiesFavoriteRepository.GetWhereFirstOrDefaultAsync(x => x.StoryId == existStory.Id && x.UserGuid == Guid.Parse(_authContext.CurrentUserId));
                if (bookmarkResult is null)
                {
                    _storiesFavoriteRepository.AddAuthented(new StoryFavorite
                    {
                        StoryId = existStory.Id,
                        UserGuid = new Guid(_authContext.CurrentUserId),
                    });
                    #region Push notifycation to author
                    await _hubContext.Clients.User(existStory.CreatedUserGuid.ToString() ?? Guid.NewGuid().ToString())
                        .SendAsync(SingleHelperConst.Instance.StreamUserSpecial, JsonConvert.SerializeObject(new BaseNotificationModels
                        {
                            NotificationContent = $"{_authContext.CurrentNameUser}-{existStory.StoryTitle}",
                            TimeCreated = DateTime.UtcNow.ToString("MM/dd"),
                            Url = existStory.ImgUrl,
                            Type = NotificationType.BookmarkStory
                        }), cancellationToken: cancellationToken);

                    #endregion

                    #region Save notification to db
                    var storyNotification = new StoryNotifications()
                    {
                        Title = existStory.StoryTitle,
                        Message = $"{_authContext.CurrentNameUser}-{existStory.StoryTitle}",
                        ImgUrl = existStory.ImgUrl,
                        NotificationUrl = "notification/user",
                        StoryId = existStory.Id,
                        UserGuid = Guid.Parse(_authContext.CurrentUserId),
                        NotificationSate = EnumStateNotification.SENT,
                        NotificationType = NotificationType.BookmarkStory

                    };
                    _storyNotificationRepository.AddAuthented(storyNotification);
                    await _storyNotificationRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
                    #endregion
                    await _storiesFavoriteRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
                    methodResult.StatusCode = StatusCodes.Status200OK;
                    methodResult.Result = true;
                    return methodResult;
                }
                var bookmarkRemove = new DeleteBookmarkStoryCommand()
                {
                    BookmarkId = bookmarkResult.Id
                };
                var result = await _mediator.Send(bookmarkRemove, cancellationToken).ConfigureAwait(false);
                methodResult.StatusCode = StatusCodes.Status200OK;
                methodResult.Result = result.Result;
                return methodResult;
                #endregion
            }
            catch (CustomException ex)
            {
                Log.Error(ex.Message, ex);
                methodResult.StatusCode = StatusCodes.Status400BadRequest;
                _logger.LogError($" -->(Bookmark story) STEP CUSTOMEXCEPTION --> {ex} ---->");
                methodResult.AddResultFromErrorList(ex.ErrorMessages);
                methodResult.Result = false;
                return methodResult;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                methodResult.StatusCode = StatusCodes.Status400BadRequest;
                _logger.LogError($" -->(Bookmark story) STEP EXEPTION MESSAGE -->{ex} ---->");
                _logger.LogError($" -->(Bookmark story) STEP EXEPTION STACK -->{ex.StackTrace} ---->");
                methodResult.AddErrorMessage(Helpers.GetExceptionMessage(ex), ex.StackTrace ?? "");
                methodResult.Result = false;
                return methodResult;
            }

        }
    }
}
