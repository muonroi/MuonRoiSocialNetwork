﻿using AutoMapper;
using BaseConfig.EntityObject.Entity;
using BaseConfig.Exeptions;
using BaseConfig.Infrashtructure;
using BaseConfig.MethodResult;
using MediatR;
using MuonRoiSocialNetwork.Application.Commands.Base.Stories;
using Serilog;
using MuonRoiSocialNetwork.Domains.Interfaces.Commands.Stories;
using MuonRoiSocialNetwork.Domains.Interfaces.Queries.Stories;
using MuonRoi.Social_Network.Storys;
using MuonRoi.Social_Network.Users;
using Newtonsoft.Json;
using MuonRoiSocialNetwork.Domains.Interfaces.Queries.Users;
using MuonRoiSocialNetwork.Domains.DomainObjects.Storys;
using MuonRoiSocialNetwork.Common.Enums.Storys;

namespace MuonRoiSocialNetwork.Application.Commands.Stories
{
    /// <summary>
    /// Create story recent
    /// </summary>
    public class CreateStoriesForUserCommand : IRequest<MethodResult<bool>>
    {
        /// <summary>
        /// Story id
        /// </summary>
        [JsonProperty("story-id")]
        public int StoryId { get; set; }
        /// <summary>
        /// Chapter index
        /// </summary>
        [JsonProperty("chapter-index")]
        public int ChapterIndex { get; set; }

        /// <summary>
        /// Chapter pageindex
        /// </summary>
        [JsonProperty("chapter-page-index")]
        public int ChapterPageIndex { get; set; }
        /// <summary>
        /// Chapter number
        /// </summary>
        [JsonProperty("chapter-number")]
        public int ChapterNumber { get; set; }
        /// <summary>
        /// Chapter latest location
        /// </summary>
        [JsonProperty("chapter-latest-location")]
        public double ChapterLatestLocation { get; set; }
        /// <summary>
        /// Chapter id
        /// </summary>
        [JsonProperty("chapter-latest-id")]
        public long ChapterId { get; set; }
        /// <summary>
        /// Story type
        /// </summary>
        [JsonProperty("story-type")]
        public EnumStoriesForUserType StoryType { get; set; }
    }
    /// <summary>
    /// Handler
    /// </summary>
    public class CreateStoriesForUserCommandHandler : BaseStoriesCommandHandler, IRequestHandler<CreateStoriesForUserCommand, MethodResult<bool>>
    {
        private readonly ILogger<CreateStoriesForUserCommandHandler> _logger;
        private readonly AuthContext _authContext;
        private readonly IUserQueries _userQueries;
        private readonly IStoriesForUserRepository _storyRecentRepository;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="configuration"></param>
        /// <param name="storiesQuerie"></param>
        /// <param name="storiesRepository"></param>
        /// <param name="logger"></param>
        /// <param name="authContext"></param>
        /// <param name="userQueries"></param>
        /// <param name="storyRecentRepository"></param>
        public CreateStoriesForUserCommandHandler(IMapper mapper, IConfiguration configuration, IStoriesQueries storiesQuerie, IStoriesRepository storiesRepository, ILoggerFactory logger, AuthContext authContext, IUserQueries userQueries, IStoriesForUserRepository storyRecentRepository) : base(mapper, configuration, storiesQuerie, storiesRepository)
        {
            _logger = logger.CreateLogger<CreateStoriesForUserCommandHandler>();
            _authContext = authContext;
            _userQueries = userQueries;
            _storyRecentRepository = storyRecentRepository;
        }
        /// <summary>
        /// Function handler
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<MethodResult<bool>> Handle(CreateStoriesForUserCommand request, CancellationToken cancellationToken)
        {
            var methodResult = new MethodResult<bool>();
            try
            {
                #region Valid request
                if (request is null)
                {
                    methodResult.StatusCode = StatusCodes.Status400BadRequest;
                    methodResult.AddApiErrorMessage(
                        nameof(EnumUserErrorCodes.USRC49C),
                        new[] { Helpers.GenerateErrorResult(nameof(EnumUserErrorCodes.USRC49C), EnumUserErrorCodes.USRC49C) }
                    );
                    return methodResult;
                }
                #endregion

                #region Check story is exist
                var existStory = await _storiesQueries.GetByIdAsync(request.StoryId);
                if (existStory is null)
                {
                    methodResult.StatusCode = StatusCodes.Status400BadRequest;
                    methodResult.AddApiErrorMessage(
                        nameof(EnumStoryErrorCode.ST10),
                        new[] { Helpers.GenerateErrorResult(nameof(request.StoryId), request.StoryId.ToString() ?? "") }
                    );
                    return methodResult;
                }
                #endregion

                #region Check user is exist
                var baseUserResponse = await _userQueries.GetUserModelByGuidAsync(new Guid(_authContext.CurrentUserId));
                if (baseUserResponse.Result is null)
                {
                    methodResult.StatusCode = StatusCodes.Status400BadRequest;
                    methodResult.AddApiErrorMessage(
                        nameof(EnumUserErrorCodes.USR13C),
                        new[] { Helpers.GenerateErrorResult(nameof(_authContext.CurrentUsername), _authContext.CurrentUsername ?? "") }
                    );
                    return methodResult;
                }
                #endregion

                #region Check is exist story recent
                var existStoryRecent = await _storyRecentRepository.GetWhereFirstOrDefaultAsync(x => x.StoryId == request.StoryId && x.UserGuid == Guid.Parse(_authContext.CurrentUserId));
                if (existStoryRecent is null)
                {
                    var newStoriesRecentForUser = new StoriesForUser
                    {
                        UserGuid = Guid.Parse(_authContext.CurrentUserId),
                        StoryId = existStory.Id,
                        StoriesForUserType = EnumStoriesForUserType.RECENT,
                        ChapterIndex = request.ChapterIndex,
                        ChapterPageIndex = request.ChapterPageIndex,
                        ChapterNumber = request.ChapterNumber,
                        ChapterLatestLocation = request.ChapterLatestLocation,
                        ChapterId = request.ChapterId
                    };
                    if (request.StoryType != EnumStoriesForUserType.RECENT)
                    {
                        var newStoriesForUser = new StoriesForUser
                        {
                            UserGuid = Guid.Parse(_authContext.CurrentUserId),
                            StoryId = existStory.Id,
                            StoriesForUserType = request.StoryType,
                            ChapterIndex = request.ChapterIndex,
                            ChapterPageIndex = request.ChapterPageIndex,
                            ChapterNumber = request.ChapterNumber,
                            ChapterLatestLocation = request.ChapterLatestLocation,
                            ChapterId = request.ChapterId
                        };
                        _storyRecentRepository.AddAuthented(newStoriesForUser);
                    }

                    _storyRecentRepository.AddAuthented(newStoriesRecentForUser);
                    await _storiesRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
                    methodResult.StatusCode = StatusCodes.Status200OK;
                    methodResult.Result = true;
                    return methodResult;
                }
                else if (request.StoryType != EnumStoriesForUserType.RECENT)
                {
                    var newStoriesRecentForUser = new StoriesForUser
                    {
                        UserGuid = Guid.Parse(_authContext.CurrentUserId),
                        StoryId = existStory.Id,
                        StoriesForUserType = request.StoryType,
                        ChapterIndex = request.ChapterIndex,
                        ChapterPageIndex = request.ChapterPageIndex,
                        ChapterNumber = request.ChapterNumber,
                        ChapterLatestLocation = request.ChapterLatestLocation,
                        ChapterId = request.ChapterId
                    };
                    _storyRecentRepository.AddAuthented(newStoriesRecentForUser);
                    await _storiesRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
                    methodResult.Result = true;
                    return methodResult;
                }
                else
                {
                    existStoryRecent.ChapterNumber = request.ChapterNumber;
                    existStoryRecent.ChapterIndex = request.ChapterIndex;
                    existStoryRecent.ChapterPageIndex = request.ChapterPageIndex;
                    existStoryRecent.ChapterLatestLocation = request.ChapterLatestLocation;
                    existStoryRecent.ChapterId = request.ChapterId;
                    _storyRecentRepository.Update(existStoryRecent);
                    await _storyRecentRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
                    methodResult.StatusCode = StatusCodes.Status200OK;
                    methodResult.Result = true;
                    return methodResult;
                }
                #endregion
            }
            catch (CustomException ex)
            {
                Log.Error(ex.Message, ex);
                methodResult.StatusCode = StatusCodes.Status400BadRequest;
                _logger.LogError($" -->(Create new story for user) STEP CUSTOMEXCEPTION --> {ex} ---->");
                methodResult.AddResultFromErrorList(ex.ErrorMessages);
                methodResult.Result = false;
                return methodResult;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                methodResult.StatusCode = StatusCodes.Status400BadRequest;
                _logger.LogError($" -->(Create new story for user) STEP EXEPTION MESSAGE -->{ex} ---->");
                _logger.LogError($" -->(Create new story for user) STEP EXEPTION STACK -->{ex.StackTrace} ---->");
                methodResult.AddErrorMessage(Helpers.GetExceptionMessage(ex), ex.StackTrace ?? "");
                methodResult.Result = false;
                return methodResult;
            }
        }
    }
}
