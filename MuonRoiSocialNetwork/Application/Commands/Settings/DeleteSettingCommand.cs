﻿using AutoMapper;
using BaseConfig.EntityObject.Entity;
using BaseConfig.Exeptions;
using BaseConfig.Infrashtructure;
using BaseConfig.MethodResult;
using MediatR;
using MuonRoi.Social_Network.Users;
using MuonRoiSocialNetwork.Application.Commands.Base.Settings;
using MuonRoiSocialNetwork.Common.Enums.Settings;
using MuonRoiSocialNetwork.Domains.Interfaces.Commands.Settings;
using MuonRoiSocialNetwork.Domains.Interfaces.Queries.Settings;
using Newtonsoft.Json;
using Serilog;

namespace MuonRoiSocialNetwork.Application.Commands.Settings
{
    /// <summary>
    /// Delete setting command request
    /// </summary>
    public class DeleteSettingCommand : IRequest<MethodResult<bool>>
    {
        /// <summary>
        /// Id setting
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }
    }
    /// <summary>
    /// Class handler
    /// </summary>
    public class DeleteSettingCommandHandler : SystemSettingCommandBase, IRequestHandler<DeleteSettingCommand, MethodResult<bool>>
    {
        private readonly ILogger<DeleteSettingCommandHandler> _logger;
        /// <summary>
        /// Constuctor
        /// </summary>
        /// <param name="settingQueries"></param>
        /// <param name="settingRepository"></param>
        /// <param name="mapper"></param>
        /// <param name="configuration"></param>
        /// <param name="authContext"></param>
        /// <param name="logger"></param>
        public DeleteSettingCommandHandler(ISettingQueries settingQueries, ISettingRepository settingRepository, IMapper mapper, IConfiguration configuration, AuthContext authContext, ILoggerFactory logger) : base(settingQueries, settingRepository, mapper, configuration, authContext)
        {
            _logger = logger.CreateLogger<DeleteSettingCommandHandler>();
        }

        /// <summary>
        /// Function handle
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<MethodResult<bool>> Handle(DeleteSettingCommand request, CancellationToken cancellationToken)
        {
            //step 1: check setting exist?
            //step 2: delete setting
            //step 3: result true or false
            MethodResult<bool> methodResult = new();
            try
            {
                #region Check setting exist?
                var existSetting = await _settingQueries.GetByIdAsync(request.Id);
                if (existSetting is null)
                {
                    methodResult.StatusCode = StatusCodes.Status400BadRequest;
                    methodResult.AddApiErrorMessage(
                        nameof(EnumSettingErrorType.STS01C),
                        new[] { Helpers.GenerateErrorResult(nameof(EnumSettingErrorType.STS01C), nameof(EnumSettingErrorType.STS01C)) }
                    );
                    methodResult.Result = false;
                    return methodResult;
                }
                #endregion

                #region Delete setting
                await _settingRepository.DeleteAsync(existSetting);
                await _settingRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
                #endregion

                #region result status
                methodResult.StatusCode = StatusCodes.Status200OK;
                methodResult.Result = true;
                return methodResult;
                #endregion
            }

            catch (CustomException ex)
            {
                Log.Error(ex.Message, ex);
                methodResult.StatusCode = StatusCodes.Status400BadRequest;
                _logger.LogError($" -->(delete setting command) STEP CUSTOMEXCEPTION --> {ex} ---->");
                methodResult.AddResultFromErrorList(ex.ErrorMessages);
                methodResult.Result = false;
                return methodResult;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                methodResult.StatusCode = StatusCodes.Status400BadRequest;
                _logger.LogError($" -->(delete setting) STEP EXEPTION MESSAGE --> {ex} ---->");
                _logger.LogError($" -->(delete setting) STEP EXEPTION STACK --> {ex.StackTrace} ---->");
                methodResult.AddErrorMessage(Helpers.GetExceptionMessage(ex), ex.StackTrace ?? "");
                methodResult.Result = false;
                return methodResult;
            }
        }
    }
}
