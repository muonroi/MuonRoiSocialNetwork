﻿using AutoMapper;
using BaseConfig.EntityObject.Entity;
using BaseConfig.Exeptions;
using BaseConfig.Extentions.ImageHelper;
using BaseConfig.Infrashtructure;
using BaseConfig.MethodResult;
using MediatR;
using MuonRoi.Social_Network.Users;
using MuonRoiSocialNetwork.Application.Commands.Base.Settings;
using MuonRoiSocialNetwork.Common.Enums.Settings;
using MuonRoiSocialNetwork.Common.Models.Settings.Request;
using MuonRoiSocialNetwork.Domains.DomainObjects.Settings;
using MuonRoiSocialNetwork.Domains.Interfaces.Commands.Settings;
using MuonRoiSocialNetwork.Domains.Interfaces.Queries.Settings;
using Newtonsoft.Json;
using Serilog;
using SixLabors.ImageSharp.Formats.Png;
namespace MuonRoiSocialNetwork.Application.Commands.Settings
{
    /// <summary>
    /// Request create setting
    /// </summary>
    public class CreateNewSettingCommand : SettingBannerRequest, IRequest<MethodResult<bool>>
    {
        /// <summary>
        /// Is banner or regular setting
        /// </summary>
        public bool IsBanner { get; set; }
    }
    /// <summary>
    /// Handler class
    /// </summary>
    public class CreateNewSettingCommandHandler : SystemSettingCommandBase, IRequestHandler<CreateNewSettingCommand, MethodResult<bool>>
    {
        private readonly ILogger<CreateNewSettingCommandHandler> _logger;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="settingQueries"></param>
        /// <param name="settingRepository"></param>
        /// <param name="mapper"></param>
        /// <param name="configuration"></param>
        /// <param name="authContext"></param>
        /// <param name="logger"></param>
        public CreateNewSettingCommandHandler(ISettingQueries settingQueries, ISettingRepository settingRepository, IMapper mapper, IConfiguration configuration, AuthContext authContext, ILoggerFactory logger) : base(settingQueries, settingRepository, mapper, configuration, authContext)
        {
            _logger = logger.CreateLogger<CreateNewSettingCommandHandler>();
        }

        /// <summary>
        /// Handler function
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<MethodResult<bool>> Handle(CreateNewSettingCommand request, CancellationToken cancellationToken)
        {
            MethodResult<bool> methodResult = new();
            try
            {
                if (request.IsBanner)
                {
                    var bannerUrl = new List<string>();
                    #region Validation request
                    if (request is null || !request.BannerFile.Any())
                    {
                        methodResult.StatusCode = StatusCodes.Status400BadRequest;
                        methodResult.AddApiErrorMessage(
                            nameof(EnumUserErrorCodes.USRC49C),
                            new[] { Helpers.GenerateErrorResult(nameof(EnumUserErrorCodes.USRC49C), nameof(EnumUserErrorCodes.USRC49C)) }
                        );
                        methodResult.Result = false;
                        return methodResult;
                    }
                    #endregion

                    #region Create new setting
                    var existSystemSettingType = await _settingRepository.GetWhereFirstOrDefaultAsync(x => x.Type == EnumSettingType.BANNER);
                    if (existSystemSettingType is null)
                    {
                        var settingBanner = _mapper.Map<SystemSettings>(request);
                        settingBanner.Type = EnumSettingType.BANNER;
                        foreach (var item in request.BannerFile)
                        {
                            var imageSrc = await ImageHelper.ConvertAndCompressImageAsync(_authContext.CurrentUserId, item, 1020, 411, true, PngCompressionLevel.Level1);
                            var result = await ImageHelper.Upload(_configuration, imageSrc);
                            if (!result.Any() || !result.Keys.Any())
                            {
                                methodResult.StatusCode = StatusCodes.Status400BadRequest;
                                methodResult.AddApiErrorMessage(
                                    nameof(EnumUserErrorCodes.USRC41C),
                                    new[] { Helpers.GenerateErrorResult(nameof(_authContext.CurrentUsername), _authContext.CurrentUsername ?? "") }
                                );
                                methodResult.Result = false;
                                return methodResult;
                            }
                            bannerUrl.Add(result.Keys.FirstOrDefault() ?? "");
                        }
                        settingBanner.Content = JsonConvert.SerializeObject(bannerUrl);
                        _settingRepository.AddAuthented(settingBanner);
                        await _settingRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
                        methodResult.Result = true;
                        methodResult.StatusCode = StatusCodes.Status200OK;
                        return methodResult;
                    }
                    #endregion

                    #region Update setting 
                    foreach (var item in request.BannerFile)
                    {
                        var imageSrc = await ImageHelper.ConvertAndCompressImageAsync(_authContext.CurrentUserId, item, 1020, 411, true, PngCompressionLevel.Level1);
                        var result = await ImageHelper.Upload(_configuration, imageSrc);
                        if (!result.Any() || !result.Keys.Any())
                        {
                            methodResult.StatusCode = StatusCodes.Status400BadRequest;
                            methodResult.AddApiErrorMessage(
                                nameof(EnumUserErrorCodes.USRC41C),
                                new[] { Helpers.GenerateErrorResult(nameof(_authContext.CurrentUsername), _authContext.CurrentUsername ?? "") }
                            );
                            methodResult.Result = false;
                            return methodResult;
                        }
                        bannerUrl.Add(result.Keys.FirstOrDefault() ?? "");
                    }
                    existSystemSettingType.Content = JsonConvert.SerializeObject(bannerUrl);
                    existSystemSettingType.SettingName = request.SettingName;
                    _settingRepository.Update(existSystemSettingType);
                    await _settingRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
                    var getDataSaved = await _settingRepository.GetWhereFirstOrDefaultAsync(x => x.Type == EnumSettingType.BANNER);
                    methodResult.Result = true;
                    methodResult.StatusCode = StatusCodes.Status200OK;
                    return methodResult;
                    #endregion
                }
                else
                {
                    #region Create new setting
                    var existSystemSettingType = await _settingRepository.GetWhereFirstOrDefaultAsync(x => x.Type == request.SettingType);
                    if (existSystemSettingType is null)
                    {
                        var newSetting = _mapper.Map<SystemSettings>(request);
                        newSetting.Type = request.SettingType;
                        newSetting.Content = newSetting.Content;
                        _settingRepository.AddAuthented(newSetting);
                        await _settingRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
                        methodResult.Result = true;
                        methodResult.StatusCode = StatusCodes.Status200OK;
                        return methodResult;
                    }
                    #endregion

                    #region Update setting 
                    existSystemSettingType.SettingName = request.SettingName;
                    existSystemSettingType.Type = request.SettingType;
                    existSystemSettingType.Content = request.Content;
                    _settingRepository.Update(existSystemSettingType);
                    await _settingRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
                    var getDataSaved = await _settingRepository.GetWhereFirstOrDefaultAsync(x => x.Type == request.SettingType);
                    methodResult.Result = true;
                    methodResult.StatusCode = StatusCodes.Status200OK;
                    return methodResult;
                    #endregion
                }
            }
            catch (CustomException ex)
            {
                Log.Error(ex.Message, ex);
                methodResult.StatusCode = StatusCodes.Status400BadRequest;
                _logger.LogError($" -->(Create new setting command) STEP CUSTOMEXCEPTION --> {ex} ---->");
                methodResult.AddResultFromErrorList(ex.ErrorMessages);
                methodResult.Result = false;
                return methodResult;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                methodResult.StatusCode = StatusCodes.Status400BadRequest;
                _logger.LogError($" -->(Create new setting) STEP EXEPTION MESSAGE --> {ex} ---->");
                _logger.LogError($" -->(Create new setting) STEP EXEPTION STACK --> {ex.StackTrace} ---->");
                methodResult.AddErrorMessage(Helpers.GetExceptionMessage(ex), ex.StackTrace ?? "");
                methodResult.Result = false;
                return methodResult;
            }

        }
    }
}
