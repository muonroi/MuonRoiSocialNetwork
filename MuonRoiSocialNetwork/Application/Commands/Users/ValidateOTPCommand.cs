﻿using AutoMapper;
using BaseConfig.EntityObject.Entity;
using BaseConfig.Infrashtructure;
using BaseConfig.JWT;
using BaseConfig.MethodResult;
using MediatR;
using Microsoft.Extensions.Caching.Distributed;
using MuonRoi.Social_Network.Users;
using MuonRoiSocialNetwork.Application.Commands.Base.Users;
using MuonRoiSocialNetwork.Common.Models.Users;
using MuonRoiSocialNetwork.Common.Models.Users.Response;
using MuonRoiSocialNetwork.Domains.Interfaces.Commands.Users;
using MuonRoiSocialNetwork.Domains.Interfaces.Queries.Users;
using MuonRoiSocialNetwork.Infrastructure.Helpers;
using Newtonsoft.Json;
using Serilog;

namespace MuonRoiSocialNetwork.Application.Commands.Users
{
    /// <summary>
    /// Request otp
    /// </summary>
    public class ValidateOTPCommand : IRequest<MethodResult<VerifyOTPResponse>>
    {
        /// <summary>
        /// username
        /// </summary>
        [JsonProperty("username")]
        public string Username { get; set; }
        /// <summary>
        /// Otp code sended to email user
        /// </summary>
        [JsonProperty("otp-code")]
        public string OTPCode { get; set; }
    }
    /// <summary>
    /// Class handle valid otp
    /// </summary>
    public class ValidOtpCommandCommandHandler : BaseUserCommandHandler, IRequestHandler<ValidateOTPCommand, MethodResult<VerifyOTPResponse>>
    {
        private readonly IDistributedCache _cache;
        private readonly ILogger<ValidOtpCommandCommandHandler> _logger;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="configuration"></param>
        /// <param name="userQueries"></param>
        /// <param name="userRepository"></param>
        /// <param name="authContext"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        public ValidOtpCommandCommandHandler(IMapper mapper, IConfiguration configuration, IUserQueries userQueries, IUserRepository userRepository, AuthContext authContext, IDistributedCache cache, ILoggerFactory logger) : base(mapper, configuration, userQueries, userRepository, authContext)
        {
            _cache = cache;
            _logger = logger.CreateLogger<ValidOtpCommandCommandHandler>();
        }
        /// <summary>
        /// Function handle
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<MethodResult<VerifyOTPResponse>> Handle(ValidateOTPCommand request, CancellationToken cancellationToken)
        {
            var methodResult = new MethodResult<VerifyOTPResponse>();
            GenarateJwtToken genarateJwtToken = new(_configuration);
            try
            {
                #region Check vaild username
                if (request is null || request.Username is null)
                {
                    methodResult.StatusCode = StatusCodes.Status400BadRequest;
                    methodResult.AddApiErrorMessage(
                        nameof(EnumUserErrorCodes.USR03C),
                        new[] { Helpers.GenerateErrorResult(nameof(EnumUserErrorCodes.USR03C), nameof(EnumUserErrorCodes.USR03C) ?? "") }
                    );
                    methodResult.Result = new VerifyOTPResponse() { IsVerify = false };
                    return methodResult;
                }
                #endregion

                #region Check user is exist by username
                var existUser = await _userQueries.GetByUsernameAsync(request.Username);
                if (existUser.Result is null)
                {
                    methodResult.StatusCode = StatusCodes.Status400BadRequest;
                    methodResult.AddApiErrorMessage(
                        nameof(EnumUserErrorCodes.USR02C),
                        new[] { Helpers.GenerateErrorResult(nameof(request.Username), nameof(request.Username) ?? "") }
                    );
                    methodResult.Result = new VerifyOTPResponse() { IsVerify = false };
                    return methodResult;
                }
                #endregion

                #region Valid Otp
                var otpCodeFromCache = await _cache.GetRecordStringAsync($"OTP{existUser.Result.UserName}");
                if (string.IsNullOrEmpty(otpCodeFromCache) || otpCodeFromCache != request.OTPCode)
                {
                    methodResult.StatusCode = StatusCodes.Status404NotFound;
                    methodResult.AddApiErrorMessage(
                        nameof(EnumUserErrorCodes.USRC54C),
                        new[] { Helpers.GenerateErrorResult(nameof(EnumUserErrorCodes.USRC54C), nameof(EnumUserErrorCodes.USRC54C) ?? "") }
                    );
                    methodResult.Result = new VerifyOTPResponse() { IsVerify = false };
                    return methodResult;
                }
                #endregion

                #region Genarate token
                var userResponse = _mapper.Map<UserModelResponse>(existUser.Result);
                var token = genarateJwtToken.GenarateJwt(userResponse, 3, listRoles: new List<string>() { "EDIT" });
                #endregion

                methodResult.Result = new VerifyOTPResponse() { IsVerify = true, Token = token };
                methodResult.StatusCode = StatusCodes.Status200OK;
                return methodResult;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                methodResult.StatusCode = StatusCodes.Status400BadRequest;
                _logger.LogError($" -->(Valid Otp) STEP EXEPTION MESSAGE --> Message {ex} ---->");
                _logger.LogError($" -->(Valid Otp) STEP EXEPTION STACK --> Message {ex.StackTrace} ---->");
                methodResult.AddErrorMessage(Helpers.GetExceptionMessage(ex), ex.StackTrace ?? "");
                methodResult.Result = new VerifyOTPResponse() { IsVerify = false };
                return methodResult;
            }


        }
    }
}
