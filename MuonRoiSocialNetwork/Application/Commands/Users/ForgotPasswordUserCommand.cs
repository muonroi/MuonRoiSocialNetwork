﻿using AutoMapper;
using BaseConfig.EntityObject.Entity;
using BaseConfig.MethodResult;
using MediatR;
using MuonRoi.Social_Network.Users;
using MuonRoiSocialNetwork.Infrastructure.Extentions.Mail;
using MuonRoiSocialNetwork.Infrastructure.Services;
using MuonRoiSocialNetwork.Domains.Interfaces.Commands.Users;
using MuonRoiSocialNetwork.Domains.Interfaces.Queries.Users;
using MuonRoiSocialNetwork.Application.Commands.Base.Users;
using Hangfire;
using Serilog;
using BaseConfig.Infrashtructure;
using Newtonsoft.Json;
using BaseConfig.Extentions.String;
using Microsoft.Extensions.Caching.Distributed;
using MuonRoiSocialNetwork.Common.Settings.RefreshTokenSettings;

namespace MuonRoiSocialNetwork.Application.Commands.Users
{
    /// <summary>
    /// Forgot password command request
    /// </summary>
    public class ForgotPasswordUserCommand : IRequest<MethodResult<bool>>
    {
        /// <summary>
        /// Username of user get password
        /// </summary>
        [JsonProperty("username")]
        public string? Username { get; set; }
    }
    /// <summary>
    /// Handler forgot password
    /// </summary>
    public class ForgotPasswordUserCommandHandler : BaseUserCommandHandler, IRequestHandler<ForgotPasswordUserCommand, MethodResult<bool>>
    {
        private readonly IDistributedCache _cache;
        private readonly IEmailService _emailService;
        private readonly ILogger<ForgotPasswordUserCommandHandler> _logger;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="configuration"></param>
        /// <param name="userQueries"></param>
        /// <param name="userRepository"></param>
        /// <param name="emailService"></param>
        /// <param name="logger"></param>
        /// <param name="authContext"></param>
        /// <param name="cache"></param>
        public ForgotPasswordUserCommandHandler(IMapper mapper, IConfiguration configuration, IUserQueries userQueries, IUserRepository userRepository, IEmailService emailService, ILoggerFactory logger, AuthContext authContext, IDistributedCache cache) : base(mapper, configuration, userQueries, userRepository, authContext)
        {
            _emailService = emailService;
            _cache = cache;
            _logger = logger.CreateLogger<ForgotPasswordUserCommandHandler>();
        }
        /// <summary>
        /// Handler function
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<MethodResult<bool>> Handle(ForgotPasswordUserCommand request, CancellationToken cancellationToken)
        {
            MethodResult<bool> methodResult = new();
            try
            {
                #region Check vaild username
                if (request is null || request.Username is null)
                {
                    methodResult.StatusCode = StatusCodes.Status400BadRequest;
                    methodResult.AddApiErrorMessage(
                        nameof(EnumUserErrorCodes.USR03C),
                        new[] { Helpers.GenerateErrorResult(nameof(EnumUserErrorCodes.USR03C), nameof(EnumUserErrorCodes.USR03C) ?? "") }
                    );
                    methodResult.Result = false;
                    return methodResult;
                }
                #endregion

                #region Check user is exist by username
                var existUser = await _userQueries.GetByUsernameAsync(request.Username);
                if (existUser.Result is null)
                {
                    methodResult.StatusCode = StatusCodes.Status400BadRequest;
                    methodResult.AddApiErrorMessage(
                        nameof(EnumUserErrorCodes.USR02C),
                        new[] { Helpers.GenerateErrorResult(nameof(request.Username), nameof(request.Username) ?? "") }
                    );
                    methodResult.Result = false;
                    return methodResult;
                }
                #endregion

                #region Send OTP to email user
                string otpCode = (StringHelpers.NextInt() % 10000).ToString("0000");
                await _cache.SetStringAsync($"OTP{existUser.Result.UserName}", otpCode, new DistributedCacheEntryOptions { AbsoluteExpirationRelativeToNow = RefreshTokenDefault.Instance.expirationTimeOtp }, cancellationToken);
                string jobId = BackgroundJob.Enqueue<ForgotPasswordUserCommandHandler>(x => x.SendEmailConfirmationEmail(existUser.Result, otpCode));
                RecurringJob.TriggerJob(jobId);
                #endregion

                methodResult.Result = true;
                methodResult.StatusCode = StatusCodes.Status200OK;
                return methodResult;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                methodResult.StatusCode = StatusCodes.Status400BadRequest;
                _logger.LogError($" -->(FORGOT PASSWORD) STEP CHECK {"Exception".ToUpper()} --> EXEPTION: {ex}");
                _logger.LogError($" -->(FORGOT PASSWORD) STEP CHECK {"Exception".ToUpper()} --> EXEPTION{" StackTrace".ToUpper()}: {ex.StackTrace}");
                methodResult.AddErrorMessage(Helpers.GetExceptionMessage(ex), ex.StackTrace ?? "");
                methodResult.Result = false;
                return methodResult;
            }

        }
        /// <summary>
        /// Send mail function
        /// </summary>
        /// <param name="user"></param>
        /// <param name="otp"></param>
        /// <returns></returns>
        public async Task SendEmailConfirmationEmail(AppUser user, string otp)
        {
            UserEmailOptions options = new()
            {
                ToEmails = new List<string>() { user.Email ?? "" },
                PlaceHolders = new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("{{UserName}}", user.UserName),
                    new KeyValuePair<string, string>("{{OTP}}",
                        string.Format(otp))
                }
            };
            await _emailService.SendEmailForForgotPassword(options);
        }
    }
}
