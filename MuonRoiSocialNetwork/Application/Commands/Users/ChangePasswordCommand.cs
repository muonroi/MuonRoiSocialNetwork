﻿using AutoMapper;
using BaseConfig.EntityObject.Entity;
using BaseConfig.Infrashtructure;
using BaseConfig.MethodResult;
using MediatR;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.IdentityModel.Tokens;
using MuonRoi.Social_Network.Users;
using MuonRoiSocialNetwork.Application.Commands.Base.Users;
using MuonRoiSocialNetwork.Domains.Interfaces.Commands.Users;
using MuonRoiSocialNetwork.Domains.Interfaces.Queries.Users;
using MuonRoiSocialNetwork.Infrastructure.Helpers;
using Newtonsoft.Json;
using Serilog;
using System.Text.RegularExpressions;

namespace MuonRoiSocialNetwork.Application.Commands.Users
{
    /// <summary>
    /// Handler request change password
    /// </summary>
    public class ChangePasswordCommand : IRequest<MethodResult<bool>>
    {
        /// <summary>
        /// Make sure user is change password
        /// </summary>
        [JsonProperty("otp")]
        public string Otp { get; set; }
        /// <summary>
        /// Check is user forgot password
        /// </summary>
        [JsonProperty("is_forgot")]
        public bool IsForgot { get; set; }
        /// <summary>
        /// if user forgot password then use username
        /// </summary>
        [JsonProperty("username")]
        public string? Username { get; set; }
        /// <summary>
        /// New password
        /// </summary>
        [JsonProperty("newPassword")]
        public string? NewPassword { get; set; }
        /// <summary>
        /// confirm password
        /// </summary>
        [JsonProperty("confirmPassword")]
        public string? ConfirmPassword { get; set; }
    }
    /// <summary>
    /// Handler command
    /// </summary>
    public class ChangePasswordCommandHandler : BaseUserCommandHandler, IRequestHandler<ChangePasswordCommand, MethodResult<bool>>
    {
        private readonly ILogger<ChangePasswordCommandHandler> _logger;
        private readonly IDistributedCache _cache;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="configuration"></param>
        /// <param name="userQueries"></param>
        /// <param name="userRepository"></param>
        /// <param name="logger"></param>
        /// <param name="cache"></param>
        /// <param name="authContext"></param>
        public ChangePasswordCommandHandler(IMapper mapper, IConfiguration configuration, IUserQueries userQueries, IUserRepository userRepository, ILoggerFactory logger, AuthContext authContext, IDistributedCache cache) : base(mapper, configuration, userQueries, userRepository, authContext)
        {
            _logger = logger.CreateLogger<ChangePasswordCommandHandler>();
            _cache = cache;
        }
        /// <summary>
        /// Function handler
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<MethodResult<bool>> Handle(ChangePasswordCommand request, CancellationToken cancellationToken)
        {
            MethodResult<bool> methodResult = new();
            try
            {
                #region Case user forgot password
                if (request.IsForgot)
                {
                    #region Valid Otp
                    var otpCodeFromCache = await _cache.GetRecordStringAsync($"OTP{request.Username}");
                    if (string.IsNullOrEmpty(otpCodeFromCache) || otpCodeFromCache != request.Otp)
                    {
                        methodResult.StatusCode = StatusCodes.Status404NotFound;
                        methodResult.AddApiErrorMessage(
                            nameof(EnumUserErrorCodes.USRC54C),
                            new[] { Helpers.GenerateErrorResult(nameof(EnumUserErrorCodes.USRC54C), nameof(EnumUserErrorCodes.USRC54C) ?? "") }
                        );
                        methodResult.Result = false;
                        return methodResult;
                    }
                    await _cache.RemoveAsync($"OTP{request.Username}");
                    #endregion

                    #region Check vaild username
                    if (request is null || request.Username is null)
                    {
                        methodResult.StatusCode = StatusCodes.Status400BadRequest;
                        methodResult.AddApiErrorMessage(
                            nameof(EnumUserErrorCodes.USR03C),
                            new[] { Helpers.GenerateErrorResult(nameof(EnumUserErrorCodes.USR03C), nameof(EnumUserErrorCodes.USR03C) ?? "") }
                        );
                        methodResult.Result = false;
                        return methodResult;
                    }
                    #endregion

                    #region Check user is exist by username
                    var userInfo = await _userQueries.GetByUsernameAsync(request.Username);
                    if (userInfo.Result is null)
                    {
                        methodResult.StatusCode = StatusCodes.Status400BadRequest;
                        methodResult.AddApiErrorMessage(
                            nameof(EnumUserErrorCodes.USR02C),
                            new[] { Helpers.GenerateErrorResult(nameof(request.Username), nameof(request.Username) ?? "") }
                        );
                        methodResult.Result = false;
                        return methodResult;
                    }
                    #endregion
                }

                #endregion

                #region Validator data
                if (!request.IsForgot)
                {
                    var existUser = await _userRepository.ExistUserByGuidAsync(Guid.Parse(_authContext.CurrentUserId));
                    if (!existUser.Result || request is null)
                    {
                        methodResult.StatusCode = StatusCodes.Status400BadRequest;
                        methodResult.AddApiErrorMessage(
                            nameof(EnumUserErrorCodes.USR02C),
                            new[] { Helpers.GenerateErrorResult(nameof(EnumUserErrorCodes.USR02C), nameof(EnumUserErrorCodes.USR02C) ?? "") }
                        );
                        methodResult.Result = false;
                        return methodResult;
                    }
                }

                if (request.NewPassword.IsNullOrEmpty() || request.NewPassword.IsNullOrEmpty())
                {
                    methodResult.StatusCode = StatusCodes.Status400BadRequest;
                    methodResult.AddApiErrorMessage(
                        nameof(EnumUserErrorCodes.USR06C),
                        new[] { Helpers.GenerateErrorResult(nameof(EnumUserErrorCodes.USR06C), nameof(EnumUserErrorCodes.USR06C) ?? "") }
                    );
                    methodResult.Result = false;
                    return methodResult;
                }
                string pwdPattern = @"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";
                bool isComplex = Regex.IsMatch(request.ConfirmPassword ?? "", pwdPattern);
                if (!isComplex)
                {
                    methodResult.StatusCode = StatusCodes.Status400BadRequest;
                    methodResult.AddApiErrorMessage(
                        nameof(EnumUserErrorCodes.USR17C),
                        new[] { Helpers.GenerateErrorResult(nameof(request.ConfirmPassword), request.ConfirmPassword ?? "") }
                    );
                    methodResult.Result = false;
                    return methodResult;
                }
                #endregion

                #region Change password
                string salt = GenarateSalt();
                string passwordHash = HashPassword(request.ConfirmPassword ?? "", salt);
                var checkStatus = await _userRepository.UpdatePassworAsync(Guid.Parse(_authContext.CurrentUserId), salt, passwordHash);
                if (!checkStatus.Result)
                {
                    methodResult.StatusCode = StatusCodes.Status400BadRequest;
                    methodResult.AddApiErrorMessage(
                        nameof(EnumUserErrorCodes.USRC50C),
                        new[] { Helpers.GenerateErrorResult(nameof(EnumUserErrorCodes.USRC50C), nameof(EnumUserErrorCodes.USRC50C) ?? "") }
                    );
                    methodResult.Result = false;
                    methodResult.StatusCode = StatusCodes.Status400BadRequest;
                    return methodResult;
                }
                #endregion

                methodResult.StatusCode = StatusCodes.Status200OK;
                methodResult.Result = true;
                return methodResult;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                _logger.LogError($" -->(CHANGE PASSWORD) STEP CHECK {"Exception".ToUpper()} --> EXEPTION: {ex}");
                _logger.LogError($" -->(CHANGE PASSWORD) STEP CHECK {"Exception".ToUpper()} --> EXEPTION{" StackTrace".ToUpper()}: {ex.StackTrace}");
                methodResult.AddErrorMessage(Helpers.GetExceptionMessage(ex), ex.StackTrace ?? "");
                methodResult.Result = false;
                return methodResult;
            }

        }
    }
}
