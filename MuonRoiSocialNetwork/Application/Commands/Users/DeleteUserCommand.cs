﻿using AutoMapper;
using BaseConfig.EntityObject.Entity;
using BaseConfig.Infrashtructure;
using BaseConfig.MethodResult;
using Hangfire;
using MediatR;
using Microsoft.Extensions.Caching.Distributed;
using MuonRoi.Social_Network.Users;
using MuonRoiSocialNetwork.Application.Commands.Base.Users;
using MuonRoiSocialNetwork.Common.Models.Users.Base.Response;
using MuonRoiSocialNetwork.Domains.Interfaces.Commands.Users;
using MuonRoiSocialNetwork.Domains.Interfaces.Queries.Users;
using MuonRoiSocialNetwork.Infrastructure.Repositories.Users;
using Serilog;

namespace MuonRoiSocialNetwork.Application.Commands.Users
{
    /// <summary>
    /// Request delete user
    /// </summary>
    public class DeleteUserCommand : IRequest<MethodResult<bool>>
    { }
    /// <summary>
    /// Handler delete user
    /// </summary>
    public class DeleteUserCommandHandler : BaseUserCommandHandler, IRequestHandler<DeleteUserCommand, MethodResult<bool>>
    {
        private readonly IDistributedCache _cache;
        private readonly ILogger<DeleteUserCommandHandler> _logger;
        private readonly IBackgroundJobClient _backgroundJobClient;
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="configuration"></param>
        /// <param name="userQueries"></param>
        /// <param name="userRepository"></param>
        /// <param name="logger"></param>
        /// <param name="authContext"></param>
        /// <param name="cache"></param>
        /// <param name="backgroundJobClient"></param>
        public DeleteUserCommandHandler(IMapper mapper, IConfiguration configuration, IUserQueries userQueries, IUserRepository userRepository, ILoggerFactory logger, AuthContext authContext, IDistributedCache cache, IBackgroundJobClient backgroundJobClient) : base(mapper, configuration, userQueries, userRepository, authContext)
        {
            _logger = logger.CreateLogger<DeleteUserCommandHandler>();
            _cache = cache;
            _backgroundJobClient = backgroundJobClient;
        }
        /// <summary>
        /// Function handle
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<MethodResult<bool>> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            MethodResult<bool> methodResult = new();
            try
            {
                #region Check valid request
                if (request is null)
                {
                    methodResult.StatusCode = StatusCodes.Status400BadRequest;
                    methodResult.AddApiErrorMessage(
                        nameof(EnumUserErrorCodes.USRC43C),
                        new[] { Helpers.GenerateErrorResult(nameof(EnumUserErrorCodes.USRC43C), nameof(EnumUserErrorCodes.USRC43C) ?? "") }
                    );
                    methodResult.Result = false;
                    return methodResult;
                }
                #endregion

                #region Check is exist user
                MethodResult<BaseUserResponse> appUser = await _userQueries.GetUserModelByGuidAsync(Guid.Parse(_authContext.CurrentUserId));
                if (appUser.Result is null)
                {
                    methodResult.StatusCode = StatusCodes.Status400BadRequest;
                    methodResult.AddApiErrorMessage(
                        nameof(EnumUserErrorCodes.USR02C),
                        new[] { Helpers.GenerateErrorResult(nameof(_authContext.CurrentUsername), _authContext.CurrentUsername) }
                    );
                    methodResult.Result = false;
                    return methodResult;
                }
                #endregion

                #region Delete User after 5 day
                var userDelete = await _userQueries.GetByGuidAsync(appUser.Result.Id);
                if (!userDelete.IsOK || userDelete.Result is null)
                {
                    methodResult.Result = false;
                    methodResult.StatusCode = StatusCodes.Status400BadRequest;
                    return methodResult;
                }
                var userInfo = userDelete.Result;
                userInfo.Status = EnumAccountStatus.InActive;
                await _userRepository.UpdateUserAsync(userInfo);
                var jobId = _backgroundJobClient.Schedule<UserRepository>(x => x.DeleteUserAsync(Guid.Parse(_authContext.CurrentUserId)), TimeSpan.FromDays(5));
                await _cache.SetStringAsync(_authContext.CurrentUsername, jobId, new DistributedCacheEntryOptions { AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(5) }, token: cancellationToken);
                #endregion
                methodResult.Result = true;
                methodResult.StatusCode = StatusCodes.Status200OK;
                return methodResult;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                methodResult.StatusCode = StatusCodes.Status400BadRequest;
                _logger.LogError($" -->(DELETE USER) STEP CHECK {"Exception".ToUpper()} --> EXEPTION: {ex}");
                _logger.LogError($" -->(DELETE USER) STEP CHECK {"Exception".ToUpper()} --> EXEPTION{" StackTrace".ToUpper()}: {ex.StackTrace}");
                methodResult.AddErrorMessage(Helpers.GetExceptionMessage(ex), ex.StackTrace ?? "");
                methodResult.Result = false;
                return methodResult;
            }

        }
    }
}
