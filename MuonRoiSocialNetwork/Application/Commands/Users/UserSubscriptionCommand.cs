﻿using AutoMapper;
using BaseConfig.EntityObject.Entity;
using BaseConfig.EntityObject.EntityObject;
using BaseConfig.Infrashtructure;
using BaseConfig.MethodResult;
using Hangfire;
using MediatR;
using MuonRoi.Social_Network.Users;
using MuonRoiSocialNetwork.Application.Commands.Base.Users;
using MuonRoiSocialNetwork.Domains.DomainObjects.Users;
using MuonRoiSocialNetwork.Domains.Interfaces.Commands.Users;
using MuonRoiSocialNetwork.Domains.Interfaces.Queries.Users;
using MuonRoiSocialNetwork.Infrastructure.Repositories.Users;
using Serilog;

namespace MuonRoiSocialNetwork.Application.Commands.Users
{
    /// <summary>
    /// Request subscription
    /// </summary>
    public class UserSubscriptionCommand : IRequest<MethodResult<bool>>
    { }
    /// <summary>
    /// Handle class
    /// </summary>
    public class UserSubscriptionCommandHandler : BaseUserCommandHandler, IRequestHandler<UserSubscriptionCommand, MethodResult<bool>>
    {
        private readonly IUserSubscriptionRepository _userSubscriptionRepository;
        private readonly Microsoft.Extensions.Logging.ILogger _logger;
        private readonly IBackgroundJobClient _backgroundJobClient;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="configuration"></param>
        /// <param name="userQueries"></param>
        /// <param name="userRepository"></param>
        /// <param name="authContext"></param>
        /// <param name="userSubscriptionRepository"></param>
        /// <param name="logger"></param>
        /// <param name="backgroundJobClient"></param>
        public UserSubscriptionCommandHandler(IMapper mapper, IConfiguration configuration, IUserQueries userQueries, IUserRepository userRepository, AuthContext authContext, IUserSubscriptionRepository userSubscriptionRepository, ILoggerFactory logger, IBackgroundJobClient backgroundJobClient) : base(mapper, configuration, userQueries, userRepository, authContext)
        {
            _userSubscriptionRepository = userSubscriptionRepository;
            _logger = logger.CreateLogger<UserSubscriptionCommandHandler>();
            _backgroundJobClient = backgroundJobClient;
        }
        /// <summary>
        /// Function handle
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<MethodResult<bool>> Handle(UserSubscriptionCommand request, CancellationToken cancellationToken)
        {
            MethodResult<bool> methodResult = new();
            try
            {
                #region Get user info
                var userInfo = await _userQueries.GetByGuidAsync(Guid.Parse(_authContext.CurrentUserId));
                if (userInfo.Result is null || !userInfo.IsOK)
                {
                    methodResult.StatusCode = StatusCodes.Status400BadRequest;
                    methodResult.AddApiErrorMessage(
                        nameof(EnumUserErrorCodes.USR02C),
                        new[] { Helpers.GenerateErrorResult(nameof(_authContext.CurrentUsername), _authContext.CurrentUsername) }
                    );
                    return methodResult;
                }
                #endregion

                #region Check user was subscription before or not
                var existUserSubscription = await _userSubscriptionRepository.GetWhereFirstOrDefaultAsync(x => x.UserId == Guid.Parse(_authContext.CurrentUserId));
                if (existUserSubscription is not null)
                {
                    methodResult.StatusCode = StatusCodes.Status200OK;
                    methodResult.Result = true;
                    return methodResult;
                }
                var newUserSubcription = new UserSubscription
                {
                    UserId = Guid.Parse(_authContext.CurrentUserId),
                    SubscriptionTotal = 30,
                    IsActive = true
                };
                var newUserSubscriptionResult = _userSubscriptionRepository.AddAuthented(newUserSubcription);
                await _userSubscriptionRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
                _backgroundJobClient.Schedule<UserSubscriptionRepository>(x => x.InActiveSubscriptionThirdtyMonth(newUserSubscriptionResult.Id), TimeSpan.FromDays(30));
                #endregion
                methodResult.StatusCode = StatusCodes.Status200OK;
                methodResult.Result = true;
                return methodResult;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                methodResult.StatusCode = StatusCodes.Status400BadRequest;
                _logger.LogError($" -->(UserSubscriptionCommand) STEP EXEPTION MESSAGE --> Message {ex} ---->");
                _logger.LogError($" -->(UserSubscriptionCommand) STEP EXEPTION STACK --> Message {ex.StackTrace} ---->");
                methodResult.AddErrorMessage(Helpers.GetExceptionMessage(ex), ex.StackTrace ?? "");
                methodResult.Result = false;
                return methodResult;
            }
        }
    }
}
