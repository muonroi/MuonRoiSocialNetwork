﻿using AutoMapper;
using BaseConfig.Infrashtructure;
using MuonRoiSocialNetwork.Domains.Interfaces.Commands.Settings;
using MuonRoiSocialNetwork.Domains.Interfaces.Queries.Settings;

namespace MuonRoiSocialNetwork.Application.Commands.Base.Settings
{
    /// <summary>
    /// Base class setting
    /// </summary>
    public abstract class SystemSettingCommandBase
    {
        /// <summary>
        /// property get config
        /// </summary>
        protected readonly IConfiguration _configuration;
        /// <summary>
        /// Info token
        /// </summary>
        protected readonly AuthContext _authContext;
        /// <summary>
        /// property _mapper
        /// </summary>
        protected readonly IMapper _mapper;
        /// <summary>
        /// Setting query
        /// </summary>
        protected readonly ISettingQueries _settingQueries;
        /// <summary>
        /// Setting repository
        /// </summary>
        protected readonly ISettingRepository _settingRepository;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="settingQueries"></param>
        /// <param name="settingRepository"></param>
        /// <param name="mapper"></param>
        /// <param name="configuration"></param>
        /// <param name="authContext"></param>
        public SystemSettingCommandBase(ISettingQueries settingQueries, ISettingRepository settingRepository, IMapper mapper, IConfiguration configuration, AuthContext authContext)
        {
            _settingQueries = settingQueries;
            _settingRepository = settingRepository;
            _mapper = mapper;
            _configuration = configuration;
            _authContext = authContext;
        }
    }
}
