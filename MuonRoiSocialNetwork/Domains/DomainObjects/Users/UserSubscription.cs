﻿using BaseConfig.EntityObject.Entity;
using MuonRoi.Social_Network.Users;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace MuonRoiSocialNetwork.Domains.DomainObjects.Users
{
    [Table("usersubscription")]
    public class UserSubscription : Entity
    {
        [Column("user_guid")]
        public Guid UserId { get; set; }

        [Column("subscription_total")]
        public int SubscriptionTotal { get; set; }

        [Column("is_active")]
        public bool IsActive { get; set; }

        public AppUser Users { get; set; }
    }
}
