﻿using BaseConfig.EntityObject.Entity;
using MuonRoiSocialNetwork.Common.Enums.Users;
using System.ComponentModel.DataAnnotations.Schema;

namespace MuonRoiSocialNetwork.Domains.DomainObjects.Users
{
    public class Language : Entity
    {
        [Column("lang")]
        public LanguageEnum LanguageName { get; set; }
    }
}
