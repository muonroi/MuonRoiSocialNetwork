﻿using BaseConfig.EntityObject.Entity;
using MuonRoiSocialNetwork.Common.Enums.Settings;

namespace MuonRoiSocialNetwork.Domains.DomainObjects.Settings
{
    /// <summary>
    /// Setting table
    /// </summary>
    public class SystemSettings : Entity
    {
        /// <summary>
        /// Setting name
        /// </summary>
        public string SettingName { get; set; } = string.Empty;
        /// <summary>
        /// Content setting
        /// </summary>
        public string Content { get; set; } = string.Empty;
        /// <summary>
        /// Setting type
        /// </summary>
        public EnumSettingType Type { get; set; }
    }
}
