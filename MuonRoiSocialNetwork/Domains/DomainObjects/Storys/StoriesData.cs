﻿using BaseConfig.EntityObject.Entity;
using MuonRoi.Social_Network.Categories;
using MuonRoi.Social_Network.Storys;

namespace MuonRoiSocialNetwork.Domains.DomainObjects.Storys
{
    /// <summary>
    /// Stories data table
    /// </summary>
    public class StoriesData : Entity
    {
        /// <summary>
        /// Story id
        /// </summary>
        public long StoryId { get; set; }
        /// <summary>
        /// Max story chapter
        /// </summary>
        public int MaxChapter { get; set; }
        /// <summary>
        /// Url to story
        /// </summary>
        public string Url { get; set; } = string.Empty;
        /// <summary>
        /// CategoryId
        /// </summary>
        public long CategoryId { get; set; }
        /// <summary>
        /// Foreign key
        /// </summary>
        public Category Category { get; set; }
        /// <summary>
        /// Foreign key
        /// </summary>
        public Story Story { get; set; }
    }
}
