﻿using BaseConfig.EntityObject.Entity;
using MuonRoi.Social_Network.Tags;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using MuonRoi.Social_Network.Users;
using MuonRoi.Social_Network.Storys;
using MuonRoiSocialNetwork.Common.Enums.Storys;

namespace MuonRoiSocialNetwork.Domains.DomainObjects.Storys
{
    /// <summary>
    /// Stories recent
    /// </summary>
    public class StoriesForUser : Entity
    {
        /// <summary>
        /// Story guid
        /// </summary>
        [Required]
        [Column("story_id")]
        public long StoryId { get; set; }
        /// <summary>
        /// Chapter id
        /// </summary>
        [Required]
        [Column("chapter_id")]
        public long ChapterId { get; set; }
        /// <summary>
        /// User guid
        /// </summary>
        [Column("user_guid")]
        public Guid UserGuid { get; set; }
        /// <summary>
        /// Story recent type
        /// </summary>
        [Column("story_type")]
        public EnumStoriesForUserType StoriesForUserType { get; set; }
        /// <summary>
        /// Chapter id
        /// </summary>
        [Column("chapter_index")]
        public int ChapterIndex { get; set; }
        /// <summary>
        /// Chapter pageindex
        /// </summary>
        [Column("chapter_page_index")]
        public int ChapterPageIndex { get; set; }
        /// <summary>
        /// Chapter number
        /// </summary>
        [Column("chapter_number")]
        public int ChapterNumber { get; set; }
        /// <summary>
        /// Chapter latest location
        /// </summary>
        [Column("latest_location")]
        public double ChapterLatestLocation { get; set; }
        /// <summary>
        /// Foreign key
        /// </summary>
        public AppUser UserMember { get; set; }
        /// <summary>
        /// Foreign key
        /// </summary>
        public Story Story { get; set; }
    }
}
