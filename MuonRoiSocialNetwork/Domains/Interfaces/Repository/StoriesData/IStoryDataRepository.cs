﻿using BaseConfig.BaseDbContext.Common;
using StoryDataEntity = MuonRoiSocialNetwork.Domains.DomainObjects.Storys.StoriesData;
namespace MuonRoiSocialNetwork.Domains.Interfaces.Repository.StoriesData
{
    /// <summary>
    /// Declare interface
    /// </summary>
    public interface IStoryDataRepository : IRepository<StoryDataEntity>
    {
    }
}
