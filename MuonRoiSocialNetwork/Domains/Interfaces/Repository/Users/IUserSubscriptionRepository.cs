﻿using BaseConfig.BaseDbContext.Common;
using MuonRoiSocialNetwork.Domains.DomainObjects.Users;

namespace MuonRoiSocialNetwork.Domains.Interfaces.Commands.Users
{
    /// <summary>
    /// Declare interface
    /// </summary>
    public interface IUserSubscriptionRepository : IRepository<UserSubscription>
    {
        /// <summary>
        /// In active
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task InActiveSubscriptionThirdtyMonth(long input);
    }
}
