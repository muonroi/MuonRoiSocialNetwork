﻿using BaseConfig.BaseDbContext.Common;
using MuonRoiSocialNetwork.Domains.DomainObjects.Settings;

namespace MuonRoiSocialNetwork.Domains.Interfaces.Commands.Settings
{
    /// <summary>
    /// Define repository
    /// </summary>
    public interface ISettingRepository : IRepository<SystemSettings>
    {
    }
}
