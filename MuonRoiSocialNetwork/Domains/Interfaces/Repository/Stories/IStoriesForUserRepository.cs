﻿using BaseConfig.BaseDbContext.Common;
using MuonRoiSocialNetwork.Domains.DomainObjects.Storys;

namespace MuonRoiSocialNetwork.Domains.Interfaces.Commands.Stories
{
    /// <summary>
    /// Declare interface
    /// </summary>
    public interface IStoriesForUserRepository : IRepository<StoriesForUser>
    {

    }
}
