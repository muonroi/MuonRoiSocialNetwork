﻿using BaseConfig.BaseDbContext.Common;
using BaseConfig.MethodResult;
using MuonRoiSocialNetwork.Common.Enums.Storys;
using MuonRoiSocialNetwork.Common.Models.Stories.Response;
using MuonRoiSocialNetwork.Domains.DomainObjects.Storys;

namespace MuonRoiSocialNetwork.Domains.Interfaces.Queries.Stories
{
    /// <summary>
    /// Declare interface
    /// </summary>
    public interface IStoriesForUserQueries : IQueries<StoriesForUser>
    {
        /// <summary>
        /// Get stories for user
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        Task<MethodResult<PagingItemsDTO<StoryModelResponse>>> GetStoriesForUser(EnumStoriesForUserType type, int pageIndex, int pageSize);
    }
}
