﻿using BaseConfig.BaseDbContext.Common;
using BaseConfig.MethodResult;
using MuonRoi.Social_Network.Users;
using MuonRoiSocialNetwork.Common.Models.Stories.Response;
using MuonRoiSocialNetwork.Domains.DomainObjects.Storys;
using SixLabors.ImageSharp.Formats.Gif;

namespace MuonRoiSocialNetwork.Domains.Interfaces.Queries.Stories
{
    /// <summary>
    /// Declare interface
    /// </summary>
    public interface IBookmarkStoryQueries : IQueries<StoryFavorite>
    {
        /// <summary>
        /// Check eixst sstory bookmark of user
        /// </summary>
        /// <param name="userGuid"></param>
        /// <param name="bookmarkId"></param>
        /// <returns></returns>
        Task<MethodResult<BookmarkStoryModelResponse>> ExistBookmarkStoryOfUser(long bookmarkId, Guid userGuid);
    }
}
