﻿using BaseConfig.BaseDbContext.Common;
using BaseConfig.MethodResult;
using MuonRoi.Social_Network.Chapters;
using MuonRoiSocialNetwork.Common.Models.Chapter.Response;

namespace MuonRoiSocialNetwork.Domains.Interfaces.Queries.Chapters
{
    /// <summary>
    /// Define interface
    /// </summary>
    public interface IChapterQueries : IQueries<Chapter>
    {
        /// <summary>
        /// Get all chapter latest paging
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="isSetCache"></param>
        /// <returns></returns>
        Task<MethodResult<PagingItemsDTO<ChapterModelResponse>>> GetAllChapterAsync(int pageIndex, int pageSize, bool isSetCache = false);

        /// <summary>
        /// Get group chapter each view
        /// </summary>
        /// <param name="storyId"></param>
        /// <param name="fromChapterId"></param>
        /// <param name="toChapterId"></param>
        /// <param name="isSetCache"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        Task<MethodResult<IEnumerable<ChapterModelResponse>>> GetGroupChapterAsync(long storyId, int pageIndex, long fromChapterId = 0, long toChapterId = 0, bool isSetCache = false);

        /// <summary>
        /// Get list chapters of story
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="storyId"></param>
        /// <param name="isLatest"></param>
        /// <returns></returns>
        Task<MethodResult<PagingItemsDTO<ChapterPreviewResponse>>> GetListChapterOfStory(long storyId, int pageIndex = 1, int pageSize = 10, bool isLatest = false);
        /// <summary>
        /// Total chapter of story
        /// </summary>
        /// <param name="storyId"></param>
        /// <param name="isSetCache"></param>
        /// <returns></returns>
        Task<MethodResult<int>> GetTotalChapterOfStoryIdAsync(long storyId, bool isSetCache = false);
        /// <summary>
        /// Get first and last chapter
        /// </summary>
        /// <param name="storyId"></param>
        /// <returns></returns>
        Task<MethodResult<Dictionary<long, long>>> GetFirstAndLastChapterByStory(long storyId);
        /// <summary>
        /// Get list chapter and paging according by 100 chapters each chunk
        /// </summary>
        /// <param name="storyId"></param>
        /// <param name="isSetCache"></param>
        /// <returns></returns>
        Task<MethodResult<List<ChapterListPagingResponse>>> PagingChapterListByStoryId(int storyId, bool isSetCache = false);
        /// <summary>
        /// Get detail chapter
        /// </summary>
        /// <param name="chapterId"></param>
        /// <returns></returns>
        Task<MethodResult<ChapterModelResponse>> GetDetailChapterById(int chapterId);
        /// <summary>
        /// Group chapter by storyId
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="storyId"></param>
        /// <param name="isSetCache"></param>
        /// <returns></returns>
        Task<MethodResult<PagingItemsDTO<ChapterModelResponse>>> GroupChapterListByStoryId(int storyId, int pageIndex = 1, int pageSize = 100, bool isSetCache = false);
        /// <summary>
        /// Get total chapter by storyId
        /// </summary>
        /// <param name="storyId"></param>
        /// <returns></returns>
        Task<MethodResult<ChapterTotalByStoryIdResponse>> GetTotalChapterByStoryId(long storyId);
    }
}
