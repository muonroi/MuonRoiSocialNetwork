﻿using BaseConfig.BaseDbContext.Common;
using MuonRoiSocialNetwork.Domains.DomainObjects.Users;

namespace MuonRoiSocialNetwork.Domains.Interfaces.Queries.Users
{
    /// <summary>
    /// Declare interface
    /// </summary>
    public interface IUserSubscriptionQueries : IQueries<UserSubscription>
    {
    }
}
