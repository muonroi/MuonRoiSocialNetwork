﻿using BaseConfig.BaseDbContext.Common;
using MuonRoiSocialNetwork.Common.Models.Stories.Dtos;
using StoryDataEntity = MuonRoiSocialNetwork.Domains.DomainObjects.Storys.StoriesData;
namespace MuonRoiSocialNetwork.Domains.Interfaces.Queries.StoriesData
{
    /// <summary>
    /// Declare interface
    /// </summary>
    public interface IStoryDataQueries : IQueries<StoryDataEntity>
    {
        /// <summary>
        /// Get story data by id category
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<PagingItemsDTO<StoryDataDto>> GetStoryDataByCategory(int categoryId, int pageIndex, int pageSize);
    }
}
