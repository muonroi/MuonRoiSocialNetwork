﻿using BaseConfig.BaseDbContext.Common;
using BaseConfig.MethodResult;
using MuonRoiSocialNetwork.Common.Enums.Settings;
using MuonRoiSocialNetwork.Common.Models.Settings.Response;
using MuonRoiSocialNetwork.Domains.DomainObjects.Settings;

namespace MuonRoiSocialNetwork.Domains.Interfaces.Queries.Settings
{
    /// <summary>
    /// Define interface
    /// </summary>
    public interface ISettingQueries : IQueries<SystemSettings>
    {
        /// <summary>
        /// Get banner images
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        Task<MethodResult<SettingBannerResponse>> GetBannerUrlAsync(EnumSettingType type);
        /// <summary>
        /// Get setting single or all
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        Task<MethodResult<List<SettingResponse>>> GetSettingsAsync(EnumSettingType type);
    }
}
