﻿using AutoMapper;
using BaseConfig.BaseDbContext.Common;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MuonRoiSocialNetwork.Common.Models.Stories.Dtos;
using MuonRoiSocialNetwork.Common.Settings.Appsettings;
using MuonRoiSocialNetwork.Domains.Interfaces.Queries.StoriesData;
using MuonRoiSocialNetwork.Domains.Interfaces.Repository.StoriesData;
using StoryDataEntity = MuonRoiSocialNetwork.Domains.DomainObjects.Storys.StoriesData;
namespace MuonRoiSocialNetwork.Controllers.StoriesData
{
    /// <summary>
    /// Auth: PhiLe 20231130
    /// </summary>
    [ApiVersion(MainSettings.APIVersion)]
    [Route("api/v{version:apiVersion}/stories/data")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class StoriesDataController : ControllerBase
    {
        private readonly IStoryDataRepository _storyDataRepository;
        private readonly IStoryDataQueries _storyDataQueries;
        private readonly IMapper _mapper;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="storyDataRepository"></param>
        /// <param name="storyDataQueries"></param>
        /// <param name="mapper"></param>
        public StoriesDataController(IStoryDataRepository storyDataRepository, IStoryDataQueries storyDataQueries, IMapper mapper)
        {
            _storyDataRepository = storyDataRepository;
            _storyDataQueries = storyDataQueries;
            _mapper = mapper;
        }
        /// <summary>
        /// Get list story data by category id
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<PagingItemsDTO<StoryDataDto>>> GetStoriesData(int categoryId, int pageIndex, int pageSize)
        {
            var result = await _storyDataQueries.GetStoryDataByCategory(categoryId, pageIndex, pageSize);
            return Ok(result);
        }
        /// <summary>
        /// Create new story data
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateStoriesData(StoryDataDto request)
        {
            var newData = _mapper.Map<StoryDataEntity>(request);
            _storyDataRepository.Add(newData);
            await _storyDataRepository.UnitOfWork.SaveChangesAsync();
            return Ok();
        }
    }
}
