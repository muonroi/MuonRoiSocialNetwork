﻿using System.ComponentModel.DataAnnotations;

namespace MuonRoiSocialNetwork.Models
{
    /// <summary>
    /// Login view model
    /// </summary>
    public class LoginViewModel
    {
        /// <summary>
        /// Email
        /// </summary>
        [Required]
        public string Username { get; set; } = string.Empty;

        /// <summary>
        /// Password
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; } = string.Empty;

        /// <summary>
        /// Remember
        /// </summary>
        [Display(Name = "Remember me")]
        public bool RememberMe { get; set; }
    }

}
